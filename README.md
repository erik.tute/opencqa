# openCQA

openCQA is an open source tool for **C**ollaborative knowledge-based healthcare data **Q**uality **A**ssessment. 

My central idea is to base data quality assessment on applicable and shareable definitions for methods that quantify or visualize characteristics of datasets. I call these definitions measurement methods (MM). Examples for MMs could be a simple method that counts the number of values outside of a given range or a method generating a heatmap summarizing the results of other MMs per variable in the dataset. MMs should be suitable for their collaborative governance/management. The knowledge which measurement methods to apply in which use cases for which data and how to prioritize and assess MM-results should not base on the opinion of single experts, but rather on collaboratively governed knowledge. A focus of my research activities in the last years was to develop, evaluate and refine concepts to support this idea. openCQA is the reference implementation for most of these concepts, demonstrating their application and defining "how implementations of the concepts should act". 

The most important concepts from my point of view:
- MM formalization as 5-tuple: Each MM is defined by five elements, 1. some descriptive *Tags*, 2. *Domain Paths* specifying the intended input data for the MM, 3. an optional *Check* function, 4. an optional *Grouping* function and 5. a *Characterization* function that creates the final output quantifying or visualizing the characteristic of interest for the dataset.
- Knowledge bases: These compilations of MMs are used to organize MMs and to add the possibility to specify the dataset of interest, e.g. by providing an AQL-query or another suitable information. Additionally we can add some descriptive text explaining the purpose of the knoweledge base, how to use it etc. 
- MM-generation depending on datatype or based on other MMs: The reference implementation has some example knowledge bases demonstrating how MMs could be semi-automatically generated from "special MM/knowledge base definitions".
- Semi-automatical generation of MMs using certain groupings of interest, e.g. per patient, per day/day of week/month/quarter/year/any other timespan, per site etc.
- Semi-automatically deriving MMs from other representations, i.e. the reference implementation demonstrates MMs checking the constraints defined in openEHR Archetypes and Templates. Other represenstations, e.g. rule templates and knowledge tables (cf. [here](https://pubmed.ncbi.nlm.nih.gov/30741240/) ), could be another valuable source for such derived MMs.   
- Structure for ex-/importing/sharing MM-results

We already applied openCQA in a limited number of real world applications, e.g. [publication 1](https://pubmed.ncbi.nlm.nih.gov/33750371/)  , [publication 2](https://pubmed.ncbi.nlm.nih.gov/34006956/) and in a few research projects, e.g. [publication 3](https://pubmed.ncbi.nlm.nih.gov/34724930/)

ATTENTION: This is not a "ready for use" product. For example, authentication and session management are horrible. The focus of this implementation is scientific proof of concept in encapsulated environments and to demonstrate concepts as reference implementation. Feel free to contribute sound solutions for everything that is currently ugly or to use the tool but knowing its limitations.

## Prerequisites
- Current installation of ``node.js``
- Current installation of ``R`` (for statistical computing, https://www.r-project.org)
- (openEHR-platform (complying with REST-API-specifications) - although using openCQA with an openEHR platform as data source is still the prettiest way of using it, we can also apply openCQA on data in JSON format or CSV, e.g. to play around with dummy data or to assess data exports from other sources)
  - A valid user having privileges for querying of templates and data.

## Installation and configuration
1. Download or clone the repository.
1. Rename files ``blank_config.js`` and ``blank_dev.js`` to ``config.js`` and ``dev.js``.
1. Set values in ``config.js``
   1. Adapt ``exports.server`` attributes to your server running node. (Not adapting ``localhost`` might seem to work, but can cause CORS related issues.)
   1. Set ``exports.r.path`` to your ``R`` installation, e.g. ``C:\\Program Files\\R\\R-3.3.0\\bin\\R`` (note the double backslashes.)
   2. Set/adapt ``exports.openEHRServer`` values:
      1. ``REST_API_Address`` is the IP-part of your openEHR-platforms REST-API-address, e.g. ``172.24.8.54``
      2. ``REST_API_Port`` is the port part, e.g. ``8081``
   3. If desired change ``exports.server_app``values, which configure where to open the client part of the DQ-tool.

## Starting server side part of the tool
(You probably want to do this differently in the long run. This is just intended as jump start for people not familiar with node.js)
- Open command prompt and switch to the folder named ``Server app`` in the cloned/downloaded repository, e.g.:
  - ``cd C:\Users\ErikT\...\openehr-dq\Server app``
- run command ``node app.js`` to start the server-app.

## Opening the client part of the tool
- Open your web browser of choice (only tested with Firefox, Chrome seemed to work, too) and open the configured address of the client part. Default configuration is:
  - ``ip_of_server_the_node_app_runs_on:3000``)

## Using the tool
- Login with your openEHR-platform credentials and retrieve desired dataset by specifying an AQL-query and hitting ``Apply AQL``
- The rest is self-explanatory, even though i will extend this documentation some day.

### Specifying AQL right
Details in the AQL influence how useful the result set is for MM generation or mapping MMs to the dataset. Few hints to avoid simple problems:
- Provide information on archetypes.
  - ``... contains COMPOSITION C``
  - ``... contains COMPOSITION C[openEHR-EHR-COMPOSITION.report.v1]``
  - Both will work as query, but only the latter allows to assume that all compositions C are reports. Thus, aligning MMs derived from a CIM starting with a node from report will only match to the latter path.
- Include RM-types in the AQL-result set.
  - ``.../items[openEHR-EHR-CLUSTER.free_text.v0]/items[at0001, 'Visit Name']/value/value``
  - ``.../items[openEHR-EHR-CLUSTER.free_text.v0]/items[at0001, 'Visit Name']/value``
  - Only results for the latter archetype-path in an AQL-query will include a RM-type needed to derived MMs based on RM-type.

### Random impressions
Screenshot of openCQA with some results:
![Example screenshot](docs/Example screenshot.png)

5-tuple specifying an MM:
![Example MM](docs/Example generated MM.png)

Example of a pie-chart displaying the count of different values for a variable:  
![Example pie chart value levels](docs/Example pie chart.png)

Example of a heatmap summarizing value existence in variables, existence where mandatory and compliance to constraints for variables:
![Example heatmap](docs/Example heatmap.png)
