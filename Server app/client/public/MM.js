function MM(tags, domainPaths, characterization, check, group) {
	var self = this;
	self.tags; //string - tags associated with
	self.domainPaths; // array - e.g.: [{domainPath: "path in CIM", pathExtension: "a path extension", matchedPath: "path from dataset", datatype:"R datatype, e.g. numeric, logical, string", item: "item0"}]
	self.check = check; //string - value for R (statistics) variable defining which check to apply on data elements in domainPath
	self.group = group;  //string - value for R (statistics) variable defining which grouping condition to apply on data elements in domainPath
	self.characterization; //string - value for R (statistics) variable defining which method to apply to characterize subset after check and group

	self.r = ko.observable(); //string - R script operationalizing this MM
	self.hash;//hash used to identify this MM, e.g. when sending results from server
	self.result; //object - result values from applying check, group and characterization on data elements in domainPaths
	self.executionTime; //observable
	self.origin; //specifies CIM this MM comes from
	self.displayedDomainPaths;//string derived from domainPaths in self.tryAlignCIMDomainPathsWithDataPath
	self.summary;//summary text describing what MM does e.g. displayedDomainPaths + check + group + characterization
	//hint: mind the constructor at the end of the class definition
	self.visible = ko.observable(true);
	self.show_results_table = ko.observable(false);//show or hide result as table

	/*
		methods
	*/

	/*	tries to align all domainPaths from MM to given paths from dataset	- if all domainPaths can be mapped to a path from dataset the adapted MM is returned, if not returns false, because this MM is not applicable on this dataset	*/
	//FIXME MM.tryAlignCIMDomainPathsWithDataPath is too long/complex
	self.tryAlignCIMDomainPathsWithDataPath = function(iDataPaths, iAtCodes = false) {
		//now try to match
		var matchedAllDomainPaths = true;
		for (var i=0, le=self.domainPaths.length; i<le; i++)	{
			var matches = 0;
			var dataPathsIndex = -1;
			//if domainPath is a MM result or if there are no datapaths at all - consider it matched
			if ((self.is_domainPath_other_data_input(self.domainPaths[i])) || (iDataPaths.length==0)) {
				matches++;
				self.domainPaths[i].matchedPath = self.domainPaths[i].domainPath;
			}
			//match normal dataPath
			else {
				iDataPaths.forEach(function(dataPath) {
					dataPathsIndex++;
					if (dataPath == "") {
						return;
					}
					if (pathsMatch(dataPath, self.domainPaths[i].domainPath)) {
						//if pathExtension indicates to count childnodes/attributes, check dataPaths if there are any children in dataset, else don't match this MM
						if ((self.domainPaths[i].pathExtension) &&
							(self.domainPaths[i].pathExtension.countAttribute || self.domainPaths[i].pathExtension.countChildren) &&
							((iDataPaths.length<=dataPathsIndex+1) || (iDataPaths[dataPathsIndex+1].indexOf(dataPath)!=0))	)	{
						}
						else	{
							matches++;
							self.domainPaths[i].matchedPath = dataPath;
						}
					}
				});
			}
			if (matches!=1) {
				matchedAllDomainPaths = false;
			}
			if (matches>1) {
				console.warn("I am a bit confused... we got " + matches+ " matches for:");
				console.warn(self.domainPaths[i].domainPath);
			}
		}
		//when matching was successful
		if (matchedAllDomainPaths) {
			//create string to display matched domainPath including pathExtension and item
			self.set_displayed_domain_paths();
			self.set_summary();
			//return final MM
			return self;
		}
		else {
			return false;
		}

		//decides if given dataPath and givenDomainPath match
		function pathsMatch(iDataPath, iDomainPath) {
			//not all matches are hard to find, e.g. when MM is derived from rm_type of dataPath
			if (iDataPath == iDomainPath) {
				return true;
			}
			//first ensure that both paths are fully in lower case
			iDataPath = iDataPath.toLowerCase().replace(/(, )/g, ",").replace(/\\'s/g, "'s");
			iDomainPath = iDomainPath.toLowerCase().replace(/(, )/g, ",");
			//now compare from end to begin (because last part has to be the same. Start of paths can differ (e.g. one path is using full paths from root of a composition and other path starts from nested archetype. Also utils.placeholders.const_dataset_row etc. can lead to differences))
			var index = 1;
			while (
				(iDataPath.length > index) &&
				(iDomainPath.length > index) &&
				(iDomainPath[iDomainPath.length-index] == iDataPath[iDataPath.length-index])
			) {
				index++;
			}
			//now check the remainders [openEHR-EHR-COMPOSITION.report.v1]
			//just a little tweak to catch many cases (around 2/3) without all the following complex conditions
			if (index<5) {
				return false;
			}
			//matched: domainPath path is substr of the dataPath (e.g. MM is derived from an Archetype not a Template or they are an exact match except for utils.placeholders.const_dataset_row in begin of dataPath)
			else if (iDomainPath.length == index) {
				return true;
			}
			//this case should not occur, i just catch it to get noticed if i missed sth. and it happens
			else if (iDataPath.length == index) {
				console.warn("Ok, i didn't expect that, why is dataPath substr of domainPath");
				console.warn(iDataPath);
				console.warn(iDomainPath);
				return true;
			}
			//both iDataPath and iDomainPath have a non-matching begin
			else {
				//extract remainders of both paths
				iDataPathRemainder = iDataPath.substr(0,iDataPath.length-index+1);
				iDomainPathRemainder = iDomainPath.substr(0,iDomainPath.length-index+1);
				//matched: dataPath is substr of domainPath (e.g. MM is derived from Template, so domainPath starts at Template's root.)
				if (iDataPathRemainder == utils.placeholders.const_dataset_row) {
					console.warn("just to notice if it ever happens:");
					console.warn("matched: dataPath is substr of domainPath");
					console.warn(iDataPath);
					console.warn(iDomainPath);
					return true;
				}
				//catch if dataPath is more specific than domainPath (i.e. dimension defined using composition and dataPath specifies Archetype-ID)
				else if (
					(iDataPathRemainder == utils.placeholders.const_dataset_row + "[openehr-ehr-composition.report.v1]") &&
					(iDomainPathRemainder == utils.placeholders.const_dataset_row + "composition")
				) {
					return true;
				}
				//these paths don't match in any yet considered case
				else {
					if ((index>25) && (typeof utils.l == 'function')) {
						/*utils.l("Pretty long match for not matching paths...this is suspiciuos, i will log that:", 20);
						utils.l(iDataPath, 20);
						utils.l(iDomainPath, 20);
						utils.l(iDataPathRemainder, 20);
						utils.l(iDomainPathRemainder, 20);*/
					}
					return false;
				}
			}
		}

	}

	/*	set string to display as domainPaths summary for MM	*/
	self.set_summary = function(domain_path) {
		self.summary = "Domain:\n" +self.displayedDomainPaths + "\n\nCheck:\n" + self.check + "\n\nGrouping:\n" + self.group +"\n\nCharacterization:\n" + self.characterization;
	}

	/*	create string identifying the MM, e.g. in an export */
	self.get_MM_identifying_string = function(aql_paths) {
		return self.tags.replace(/,per_\w+/,"")
			+ '_ON_' + self.create_domain_path_shorthand(self.domainPaths[0], aql_paths) //indicates domainPath MM refers to
			+ '_'
			+ utils.generate_hash(self.check + '_' + self.characterization);
	}

	/*	return text for a single matched domain paths with extension e.g. .../context/start_time/value.asDaysFrom	*/
	self.single_domain_path_to_text = function(domain_path) {
		var pathExtension = (domain_path.pathExtension) ? domain_path.pathExtension.asTextToAppend : "";
		return domain_path.matchedPath + pathExtension;
	}

	/*	return a short meaningful text for a single domain paths	*/
	self.create_domain_path_shorthand = function(domain_path, aql_paths) {
		var tmp = self.single_domain_path_to_text(domain_path);
		//if path does not refer an archetype_path
		if (tmp.indexOf(utils.placeholders.const_other_data_input)>-1) {
			return utils.placeholders.const_other_data_input + utils.generate_hash(tmp);;
		}
		else {
			//extract last part of path - in combination with aql-alias and hash: start_time_453356_value.asDaysFrom
			var splits = tmp.split('/');
			var last_part = splits[splits.length-1];
			//catch if / was the last char
			if (last_part == '') {
				last_part = splits[splits.length-2];
			}
			//catch diagnose,absence,exclusion stuff - add -absence or -exclusion or -diagnosis to distinguish better
			var my_matches = /ion.absence|ion.exclusion|ion.problem_diagnosis/.exec(domain_path.matchedPath);
			var pattern_addition = "";
			if (my_matches) {
				pattern_addition = "-"+my_matches[0].replace("ion.","").replace("problem_","");
			}
			//there is lots of stuff in the middle of the path with the hash we ensure its unique
			return self.derive_alias_for_path_from_aql(tmp, aql_paths) + pattern_addition + "_" + utils.generate_hash(tmp) + "_" + last_part;
		}
	}

	/* returns alias as string if possible, else an empty string*/
	self.derive_alias_for_path_from_aql = function(domain_path, aql_paths) {
		//check if shorthand can be derived from aql-path, i.e. at least one aql_path should occur within path - if more than one aql-paths match use longest
		var longest_match = {path:""};
		aql_paths.forEach(aql_path => {
			var b = aql_path.path.toLowerCase();
			if ((domain_path.indexOf(aql_path.path.toLowerCase())>-1) && (aql_path.path.length>longest_match.path.length)) {
				longest_match = aql_path;
			}
		});
		//use found match
		if ((longest_match.path.length>0) && (longest_match.as)) {
			return longest_match.as;
		}
		else {
			return "";
		}
	}

	/*	set string to display as domainPaths for MM	*/
	self.set_displayed_domain_paths = function() {
		self.displayedDomainPaths = "";
		self.domainPaths.forEach((dp,i)=> {
			var linebreak = (i>0) ? "\n" : "";
			var itemNumber = (i>0) ? i : "";
			self.displayedDomainPaths += linebreak + "# " + dp.item + " ("+ dp.datatype +") = " + self.single_domain_path_to_text(dp);
		});
	}

	/*	Checks if given MM equals to MM - i.e. does the same while maybe coming from another origin	*/
	self.equals = function(iMM) {
		if (	(self.displayedDomainPaths == iMM.displayedDomainPaths) &&
				(self.check == iMM.check) &&
				(self.group == iMM.group) &&
				(self.characterization == iMM.characterization) ) {
			return true;
		}
		else {
			return false;
		}
	}

	/*	Checks if given MM equals to MM - but using stringified domainPaths for comparison	*/
	self.equals_raw = function(iMM) {
		if (	(JSON.stringify(self.domainPaths) == JSON.stringify(iMM.domainPaths)) &&
				(self.check == iMM.check) &&
				(self.group == iMM.group) &&
				(self.characterization == iMM.characterization) ) {
			return true;
		}
		else {
			return false;
		}
	}

	/*	resolves dependencies and initiates execution on server-side part of app 	*/
	self.execute = function(iMMs, execution_time) {
		if (execution_time) {
			self.executionTime({display: execution_time.toLocaleString(), ts: execution_time});
		}
		self.resolve_dependencies_for_MM(iMMs);
		if (self.waiting_for_dependencies) { //waiting for result of other MM - try again later.
			setTimeout(function() {
				self.execute(iMMs);
			}, 1000);
		}
		//execute on server if all data needed is available
		else {
			vm.running_MMs(vm.running_MMs()+1);
			var mm_parameters = {
				domainPaths: self.domainPaths,
				displayedDomainPaths: self.displayedDomainPaths,
				characterization: self.characterization,
				check: self.check,
				group: self.group
			}
			self.set_hash();
			self.set_R();
			self.send_execution_request_to_server(mm_parameters);
		}
	}

	/*	Sets the value for the R-code of the MM	*/
	self.set_R = function() {
		var mm_parameters = {
			domainPaths: self.domainPaths,
			displayedDomainPaths: self.displayedDomainPaths,
			characterization: self.characterization,
			check: self.check,
			group: self.group
		}
		self.r(utils.contextualizeMMTemplate(RTemplate, mm_parameters, "<<server_app_path>>/public"));
	}

	/*	Sets the hash value for the MM	*/
	self.set_hash = function() {
		dps = JSON.parse(JSON.stringify(self.domainPaths));
		dps.forEach((dp, i)=>{
			//removes inputValues in domainPaths if dp has some
			if ((dp.pathExtension) && (dp.pathExtension.asInputValues)) {
				if (dp.pathExtension.inputValues) {
					dp.pathExtension.inputValues = [];
				}
			}
		});
		var mm_parameters = {
			domainPaths: dps,
			characterization: self.characterization,
			check: self.check,
			group: self.group
		}
		self.hash = utils.generate_hash(JSON.stringify(mm_parameters));
	};

	/*	resolves dependencies, i.e. for multi-layered MMs  	*/
	self.resolve_dependencies_for_MM = function(iMMs) {
		self.waiting_for_dependencies = false;
		for (var i=0, le=self.domainPaths.length; i<le; i++) {
			if (self.is_domainPath_other_data_input(self.domainPaths[i])) {
				self.resolve_dependencies_for_domainPath(self.domainPaths[i], iMMs)
			}
		}
	}

	self.resolve_dependencies_for_domainPath = function(iDomainPath, iMMs) {
		var tmpMMs = [];
		//filter MM list by filter from domainPath (and should not wait for own results obviously)
		tmpMMs = iMMs.filter(function(iMM) {
			return ((!self.equals(iMM)) && (eval(iDomainPath.domainPath.replace(utils.placeholders.const_other_data_input,""))));
		});
		//prepare input values array
		if (tmpMMs.length>0) {
			iDomainPath.pathExtension.inputValues = [];
		}
		else {
			if (iDomainPath.pathExtension.asInputValues.indexOf(".currentTimeAsUnixTS") == 0) {
				iDomainPath.pathExtension.inputValues = [(Math.round(Date.now()/1000))];
			}
		}
		//retrieve values
		for (var i=0, le=tmpMMs.length; i<le; i++) {
			self.retrieve_values_from_dependency(tmpMMs[i], iDomainPath, iMMs);
		}
	}

	self.retrieve_values_from_dependency = function(iDependency, iDomainPath, iMMs) {
		//for size, resultsValue and dimension_level
		if ( (iDomainPath.pathExtension.asInputValues.indexOf(".resultsValue") == 0) ||
				(iDomainPath.pathExtension.asInputValues.indexOf(".size") == 0) ||
				(iDomainPath.pathExtension.asInputValues.indexOf(".dimension_level") == 0) )	{
					if (iDependency.result().results.length>0) {
						iDomainPath.pathExtension.inputValues.push(iDependency.hash);
					}
					else {
						self.waiting_for_dependencies = true;
						if (!iDependency.executionTime()) {
							iDependency.execute(iMMs, new Date());
						}
					}
		}
		//domainPath for given item
		else if (iDomainPath.pathExtension.asInputValues.indexOf(".item") == 0) {
			var tmpCompare = iDomainPath.pathExtension.asInputValues.substr(1);
			iDomainPath.pathExtension.inputValues.push(iDependency.domainPaths.find(function(dP) {return (dP.item == tmpCompare);}).matchedPath);
		}
		else {
			console.warn("Warning: Unknown path extension (for using a MM's properties in other MM): " + iDomainPath.pathExtension.asInputValues);
		}
	}

	self.send_execution_request_to_server = function(mm_parameters) {
		try {
			$.ajax({
				async: true,
				type: "POST",
				contentType:"application/json",
				url: config.server_app.protocol+config.server_app.address+":"+config.server_app.port + "/executeMM",
				data: JSON.stringify({
					mmParameters: mm_parameters,
					mm_hash: self.hash,
					connection_id: client.connection_id
				}),
				beforeSend: function (xhr) {
					xhr.setRequestHeader ("Authorization", "Basic " + config.authString);
				}
			});
		} catch (error) {
			console.error(error);
		}
	}

	/*	adds MM's results	*/
	self.add_result = function(iResults) {
		self.result(self.result().addResult(iResults, self.tags.match(/\.\w+/)));
		self.show_results_table(iResults.results.length<=config.display_options.show_results_by_default_threshold);
	}

	/*	returns a cloned MM object	*/
	self.clone = function() {
		return new MM(JSON.parse(JSON.stringify(self.getObjectValues())));
	}

	/*	prepare this object instance for serialization, i.e.: remove object functions, ko.observables -> plain JS-objects	*/
	self.getObjectValues = function(iMode=false) {
		var tmp = {};
		tmp.tags = self.tags;
		tmp.domainPaths = self.domainPaths;
		tmp.characterization = self.characterization;
		tmp.check = self.check;
		tmp.group = self.group;
		if (iMode=="state") {
			tmp.result = self.result().getObjectValues();
			tmp.executionTime = self.executionTime();
			tmp.origin = self.origin;
			tmp.displayedDomainPaths = self.displayedDomainPaths;
		}
		return tmp;
	}

	/*	checks if given domainPath is a real domain path or a path for result of a MM	*/
	self.is_domainPath_other_data_input = function(iDomainPath) {
		return (iDomainPath.domainPath.indexOf(utils.placeholders.const_other_data_input)==0) ||
		(iDomainPath.domainPath.indexOf('${utils.placeholders.const_other_data_input}')==0);
	}

	/*
		constructor
	*/
	//normal constructor from somewhere within app
	//FIXME whole MM-constructor, getObjectValues, clone construct seems smelly
	if (typeof tags == 'string') {
		self.tags = tags;
		//ensure domain path is always lower case
		self.domainPaths = domainPaths.map(function(aDomainPath) {
			if (!self.is_domainPath_other_data_input(aDomainPath)) {
				aDomainPath.domainPath = aDomainPath.domainPath.toLowerCase();
			}
			return aDomainPath;
		});
		self.characterization = characterization;
		self.check = check;
		self.group = group;
		self.result = ko.observable(new MM_result());
		self.executionTime = ko.observable(false);
		self.origin = null;
		self.displayedDomainPaths = null;
		self.summary = null;
	}
	//loading from object (probably loading from serialized state)
	else {
		self.tags = tags.tags;
		self.domainPaths = tags.domainPaths.map(function(aDomainPath) {
			if (!self.is_domainPath_other_data_input(aDomainPath)) {
				aDomainPath.domainPath = aDomainPath.domainPath.toLowerCase();
			}
			return aDomainPath;
		});
		self.characterization = tags.characterization;
		self.check = tags.check;
		self.group = tags.group;
		self.result = ko.observable(new MM_result());
		self.result.extend({ rateLimit: 100 });
		self.executionTime = (tags.executionTime) ? ko.observable(tags.executionTime) : ko.observable(false);
		self.origin = tags.origin;
		self.displayedDomainPaths = tags.displayedDomainPaths;
		self.summary = self.displayedDomainPaths + "\nCheck:\n" + self.check + "\nGrouping:\n" + self.group + + "\nCharacterization:\n" + self.characterization;
	}
}

function MM_result(iResults = [], iTS = "") {
	var self = this;
	//self.export = ko.observable(false);
	self.results = iResults;
	self.ts = iTS;
	self.localTimeString = iTS.toLocaleString();

	/*	prepare this object instance for serialization, i.e.: remove object functions, ko.observables -> plain JS-objects	*/
	self.getObjectValues = function() {
		var tmp = {};
		tmp.results = self.results;
		tmp.ts = self.ts;
		tmp.localTimeString = self.localTimeString;
		return tmp;
	}

	/*	adds results	*/
	self.addResult = function(iResults, filetype) {
		//catch empty result
		if (!iResults.results) {iResults.results = [{dimension_level:"empty_result", value:0, size:0}];}
		//catch MMs not returning values but numbers indicating filenames
		if (filetype) {
			iResults.results = iResults.results.map((result_row) => {
				result_row.value = "/plots/" + result_row.value + filetype[0];
				return result_row;
			});
		}
		var newResult = new MM_result(iResults.results, new Date());
		return newResult;
	}
}
