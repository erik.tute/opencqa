# Version: 0.5.0
# Author: Erik Tute

# Expected contextualization: (what a tool generating executable MMs has to add - contextualization values are supposed to be documented as part of the MM with the results)
# domainPaths identify the "variables" the MM refers to
# (the tool executing the MMs has to provide the data for these as vectors named item0, item1, item2, ...)
#domainPaths_replace
# (optional) function to apply on each row of data to create subgroups in a dimension
group <- NULL
# (optional) function to apply on each row of data e.g. to check if values fulfill a certain condition
check <- NULL
# function to apply on checked values (after grouping and checks were applied) to summarize input values per group e.g. a mean
characterization <- NULL

######################################## 
# generic R-code below (always the same)
########################################

# apply check
if (!is.null(check)) {
  checked = mapply(check, replace_with_items)
} else {
  checked = item0
}
# subgrouping
if (!is.null(group)) {
  grouping = mapply(group, replace_with_items)
} else {
  grouping = rep(TRUE, length(checked))
} 
#characterization
results = aggregate(checked, list(grouping), characterization)
groupSizes = aggregate(checked, list(grouping), length)
#return as JSON
sep = ''
str = ''
for(i in 1:nrow(results)){
  if (i > 1) {sep = ','}
  escaped = sub("[\r\n]", "", results[i,1])
  str = (sprintf('%s%s{"dimension_level":"%s", "value":%f, "size": %d}', str, sep, escaped, results[i,2], groupSizes[i,2]))
}
cat(sprintf('[%s]<end>', str))
