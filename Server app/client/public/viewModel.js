const const_user_label = "User";

function viewModel() {
	var self = this;
	self.aql= ko.observable();//AQL-query string which defines dataset to be retrieved and analyzed
	self.CIMs = ko.observableArray(); //list of CIMs from which MMs can be derived
	self.got_connected = ko.observable(const_user_label);
	self.dimensions = ko.observableArray(); //list of dimensions
	self.MMs = ko.observableArray(); //list of all MMs
	self.visibleMMs = ko.observable("No filter applied"); //number of currently visible MMs (after applying filter)
	self.paths_info = ko.observable(); //info on available paths after retrieving data from repository
	self.lockMMs = false; //just a helper variable to prevent async-functions from parallel working with list (thus screwing it up)
	self.edit_MM = ko.observable(false); //for editing MMs in GUI
	self.retrieving_data_completed = ko.observable(true);//indicates if all data for aql was retrieved already
	self.running_MMs = ko.observable(0);//counter for all MMs that were executed, but not have results yet
	self.max_pagesize = ko.observable(10000);
	self.paths = []//list of paths the server-app derived from the dataset (dataset = result from AQL-query)
	self.paths_infos = {};
	self.unique_paths = ko.observableArray();
	self.tags = ko.observable("");
	self.mmFilter = ko.observable("");
	self.nrFilter = ko.observable("");
	self.tagFilter = ko.observable("");
	self.executionFilter = ko.observable("");
	self.domainPathFilter = ko.observable("");
	self.kbMMs = ko.observableArray();//MMs from knowledge base

	/*	adds set of given MMs from given origin to self.MMs (if applicable on dataset and no duplicate)	*/
	self.add_MMs = function(iMMs, iOrigin, iAtCodes = false) {
		//var obj = {};
		iMMs.forEach(function(current_MM, i) {
			//try to align MM with dataset (see if MM's domainPaths match to datasetPaths), if yes also adds some values (matched path, display texts etc.)
			current_MM = current_MM.tryAlignCIMDomainPathsWithDataPath(self.paths, iAtCodes);
			if ( (current_MM) &&
			//check if there is already an euqal MM in set
			(!self.MMs().find(function(MMFromList) {
				return (current_MM.equals(MMFromList));
			})) ) {
				//add origin
				current_MM.origin = iOrigin;
				//actually add MM to MM set
				//current_MM.set_hash();
				//obj[current_MM.hash] = current_MM.get_MM_identifying_string([]).replace(/_?_-?\d+/g,"").replace(/,/g,"-");
				self.MMs.push(current_MM);
			}
			else {
				if (current_MM) {
					console.log("current_MM is duplicate of MM already in list:");
				}
				else {
					console.log("current_MM does not match current data:");
				}
				console.log(iMMs[i]);
			}
		});
		//console.log(JSON.stringify(obj));
	}

	/*	set MM to be edited	*/
	self.set_edit_MM = function(sender, event) {
		self.edit_MM(sender);
	}

	/*	save changes from editing MM	*/
	self.save_MM_changes = function(todo) {
		//if user clicked save button update MM with edits
		if (todo == "save") {
			var tmp_MM = new MM(self.edit_MM().tags, JSON.parse(document.getElementById("edit_MM_domainPaths").value), self.edit_MM().characterization, self.edit_MM().check, self.edit_MM().group);
			if (tmp_MM.check=="") {tmp_MM.check = null;}
			if (tmp_MM.group=="") {tmp_MM.group = null;}
			tmp_MM = tmp_MM.tryAlignCIMDomainPathsWithDataPath(self.paths);
			if (tmp_MM){
				self.MMs.replace(self.edit_MM(), tmp_MM);
			}
			else {
				console.warn("Aligning edited MM with dataPaths failed!");
			}
		}
		//whatever user clicked (save or abort) - hide the div for editing MMs
		self.edit_MM(false);
	}

	/*	delete MM	*/
	self.delete_MM = function() {
		if(window.confirm("Do you really want to delete  this MM?")) {
			self.MMs.remove(self.edit_MM());
		}
		//hide the div for editing MMs
		self.edit_MM(false);
	}

	/*	delete MMs	*/
	self.delete_MMs = function() {
		if(window.confirm("Do you really want to delete all currently displayed MMs?")) {
			self.MMs.remove(aMM => {return aMM.visible()});
		}
	}

	/*	Clone given MM	*/
	self.clone_MM = function(sender, event) {
		var tmp_MM = sender.clone();
		tmp_MM.tags = sender.tags + ',cloned_MM';
		self.MMs.push(tmp_MM.tryAlignCIMDomainPathsWithDataPath(self.paths));
	}

	//TODO 4 - retrieving and joining data from several sources and queries for DQA would be useful in some use cases

	/* - TODO 4 - improve GUI handling of datapaths
		- support making use of app.derive_AQL_for_rm_types via GUI etc. (to retrieve more rm_types with another query) - probably it is a good idea to try to retrieve 1 whole composition
		- e.g. for filtering or for dimension levels paths should be selectable somehow somewhere
		- since cardinality info from CIM does not provide this info really good a simple possibility to mark if variable is mandatory would be nice
			- representation_complete + ref_value for it's result provides that info in an ideal world...
		- possibility to mark for paths what kind of variable it is in a use case (e.g. confounder, outcome, etc.)
	*/
	/*	Tries to set content from paths_infos textarea as vm.paths_infos*/
	self.set_as_paths_infos = function() {
		try{
			$("#paths_elements")[0].style.display="none";
			self.paths_infos = JSON.parse($("#paths_infos").val());
			client.show_hint("Setting paths infos.", 500);
		} catch(error) {
			console.error(error);
			client.show_hint("Setting paths_infos failed!", 3000);
		}
	}

	/*	sets JSON-string of current paths_infos as value of paths_infos textarea	*/
	self.load_paths_infos = function() {
		$("#paths_infos").val(JSON.stringify(self.paths_infos, null, 3));
	}

	/* TODO 8 - Big data ready. Ideas:
		- maybe its a good idea to not hold all data in one object in memory in app.js?
			- need a proper session handling solution instead of global.contexts anyway
		- maybe plain text via sockets is not the best solution for passing big amounts of data to R?
	*/
	//TODO 6 - idea - conditions for MM execution... not even firing MM X when domainPathY violates some criteria - just another path extension named condition? Sollte vermutlich ganz gut klappen - cf. https://dx.doi.org/10.3205/19gmds012 Beispiel: Fehlende Werte, die mit 999 kodiert sind. Die würden natürlich Verteilungen "versauen". ) Ist aber auch kein Drama wenn man sie halt "vergeblich" berechnet.

	/* TODO 5 - add more example MMs
		- scatter plots are common and there are existing R functions
		- Pareto charts (barchart with descending order of occurence) are useful in some cases? (There should be existing solutions in R)
		- comparing two columns if values are concordant
		- MM checking for extreme values for continuous/numeric variables
			- Domain analysis identifies a domain or set of commonly used values within the attribute by capturing the most frequently occurring values. For example, the Status column in the Customers table is profiled and the results reveal that 90% of the values are among the following: "MARRIED", "SINGLE", "DIVORCED". Further analysis and drilling down into the data reveal that the other 10% contains misspelled versions of these words with few exceptions
		- Attribute dependency rule, "e.g., women should not have a diagnosis of prostate cancer" (Brown2013)
		- example MM for state dependent temporal rule, "e.g., a series of prenatal ultrasounds should precede a pregnancy" (Brown2013)
		- value distribution compared to an expected distribution
		- using null flavor information, e.g. kind of missingness
		- better visualization for duration distributions - for example age looks like P44Y and will currently be converted to seconds...
	*/
	//TODO 3 in complex templates paths can include stuff like "content", "items" or "data" also in the middle not in the beginning...currently I can't deal with that correctly (example: Anamnese.opt if AQL just selects blood pressure somewhere before the data part or risk factor paths)
	// TODO 6 MM-set Zusammenstellung und Ausführung über GUI aber als Script "anzeigen/persistierbar/bearbeitbar/reproduzierbar" machen, ähnlich Open refine - (in so einem Skript wären dann ja auch Bedingungen für execution usw. möglich

	/* retrieve DQ knowledge from knowlege base */
	self.get_knowledge_base = async function(sender, event) {
		self.kbMMs.removeAll();
		var kbs;
		try {
			kbs = await $.ajax({
				async: true,
				type: "GET",
				contentType:"application/json",
				url: config.server_app.protocol+config.server_app.address+":"+config.server_app.port + "/kb",
			});
			//add knowledge-bases to list
			kbs.forEach(kb => {
				self.kbMMs.push(kb);
			});
			self.kbMMs.sort(function (left, right) { return left.order == right.order ? 0 : (left.order < right.order ? -1 : 1) });
			return self.kbMMs().length;
		} catch (error) {
			console.error(error);
    }
	}

	/*	Clicked knowledge base from KB-list - try to add MMs from this KB	*/
	self.clicked_kb = function(sender, event) {
		if ((sender.aql)
		&& (sender.aql !=self.aql())
		&& (confirm("Set AQL from knowledge-base? \n\n"+sender.aql))) {
			self.aql(sender.aql);
			$("#AQL_elements")[0].style.display = "block";
			return;
		}
		//event.target.parentNode.className = "selected";//mark as already used
		var tmp_kb_MMs = [];
		var derive = (self.paths.length==0) ? false : sender.meta_kb;
		sender.MMs.forEach(function(iMM) {
			if (derive) {
				tmp_kb_MMs.push.apply(tmp_kb_MMs, eval(derive));
			}
			else {
				var tmp_MM = new MM(iMM);
				tmp_kb_MMs.push(tmp_MM);
			}
		});
		self.add_MMs(tmp_kb_MMs, "KB_"+sender.kb_tag);
	}

	/*	derives a new MM for each visible MM using given MM from knowledge base	*/
	self.derive_new_MM_per_MM = function(kb_MM) {
		var MMs_to_add = [];
		self.MMs().forEach((aMM, index) => {
			try {
				if (aMM.visible()) {
					var mapped_domain_paths = JSON.parse(JSON.stringify(kb_MM.domainPaths));
					mapped_domain_paths.map((dP) => {
						dP.domainPath = eval("`"+dP.domainPath+"`");
						return dP;
					})
					MMs_to_add.push(new MM(
						eval("`"+kb_MM.tags+"`"),
						mapped_domain_paths,
						eval("`"+kb_MM.characterization+"`"),
						kb_MM.check,
						kb_MM.group
					));
				}
			} catch (error) {
				console.error(error);
			}
		});
		return MMs_to_add;
	}

	/*	for each path with known datatype derives suitbable MMs from knowledge base	*/
	self.derive_new_MM_per_path = function(kb_MM) {
		var MMs_to_add = [];
		var types_of_interest = eval("`"+kb_MM.domainPaths[0].domainPath+"`")
			.replace(utils.placeholders.const_other_data_input,"")
			.trim()
			.split(",");
		for (var path in self.paths_infos)  {
			try {
				if (types_of_interest.includes(self.paths_infos[path].datatype)) {
					var datatype =
						(["Integer", "Real", "ISO8601","ISO8601_duration"].includes(self.paths_infos[path].datatype)) ? "numeric" :
						(self.paths_infos[path].datatype=="Boolean") ? "logical" :
						"string";
					var pathExtension =
						(["ISO8601"].includes(self.paths_infos[path].datatype)) ?
						{asUnixTS: true, asTextToAppend: ".asUnixTS"} :
						(["ISO8601_duration"].includes(self.paths_infos[path].datatype)) ? {durationIn: "seconds", asTextToAppend: ".durationIn(seconds)"} :
						null;
					var mapped_domain_paths = JSON.parse(JSON.stringify(kb_MM.domainPaths));
					mapped_domain_paths[0] = {
						domainPath: path,
						datatype: datatype,
						pathExtension: pathExtension,
						item: "item0"
					};
					MMs_to_add.push(new MM(
						eval("`"+kb_MM.tags+"`"),
						mapped_domain_paths,
						eval("`"+kb_MM.characterization+"`"),
						kb_MM.check,
						kb_MM.group
					));
				}
			} catch (error) {
				console.error(error);
			}
		}
		return MMs_to_add;
	}

	/*	gets the MM as JSON string	*/
	self.get_MM_as_JSON = function() {
		var mm_as_JSON = utils.clean_domain_paths(JSON.stringify(self.edit_MM().getObjectValues("knowledge_base"), null, 3),"MM",true);
		navigator.clipboard.writeText(mm_as_JSON);
		console.log(mm_as_JSON);
	}

	/*	logs the MM in console	*/
	self.log_MM_to_console = function() {
		console.log(self.edit_MM());
	}

	/*	gets the MM as R-script with dummy data vectors (for easier debugging in R-studio)	*/
	self.get_MM_R = function() {
		var tmp_MM = self.edit_MM().clone();
		tmp_MM.displayedDomainPaths = '#dummy data';
		tmp_MM.domainPaths.forEach((dP, i) => {
			tmp_MM.displayedDomainPaths += '\n' + dP.item + ' = ';
			if (dP.datatype == "numeric") {
				tmp_MM.displayedDomainPaths += "c(1,2,3,4,NA,5,6,7,8,9)";
			}
			else if (dP.datatype == "string") {
				tmp_MM.displayedDomainPaths += 'c("a","b","c","d",NA,"e","f","g","h","i")';
			}
			else {
				tmp_MM.displayedDomainPaths += 'c(TRUE,FALSE,TRUE,FALSE,NA,TRUE,FALSE,TRUE,FALSE,TRUE)';
			}
		});
		tmp_MM.set_R();
		navigator.clipboard.writeText(tmp_MM.r());
		console.log(tmp_MM.r());
	}



	//TODO 3 AQL FROM EHR[ehr_id/value ='xyz'] leads to invalid query even if it is valid

	/*	Export MMs as KB	*/
	self.export_MMs_as_KB = function(with_AQL = false) {
		var tmp = [];
		self.MMs().forEach(iMM=>{
			if (iMM.visible()) {
				tmp.push(iMM.getObjectValues("knowledge_base"));
			}
		});
		var structure = "MM_array";
		if (with_AQL) {
			structure = "KB";
			tmp = {aql:self.aql(),MMs:tmp}
		}
		//actually save
		utils.saveToFile("kb_export_openCQA.json", utils.clean_domain_paths(JSON.stringify(tmp), structure));
	}

	/*	Adds results which were dragged and dropped into the GUI - so that suitable MMs can display the results	tables*/
	self.import_MMs_results_JSON = function(iString, iFilename) {
		var tmp_results = JSON.parse(iString);
		//for each result in file
		tmp_results.forEach((iResult)=>{
			var tmp_index = false;
			if (iResult.hash) {
				//find suitable MM
				tmp_index = self.MMs().findIndex(iMM=>{
					if (!iMM.hash) {
						iMM.set_hash();
						iMM.set_R();
					}
					return (iResult.hash == iMM.hash);
				});
			}
			//matching hashes did not work, e.g. because matched paths may change the hash (and are intended to do so) - now, if we executed our MMs on a dataset and now want to look at the results without having the dataset available - matching hashed might fail - so we try to match results using tags
			if ((!tmp_index) || (tmp_index==-1)) {
				tmp_index = self.MMs().findIndex(iMM=>{
					return (iResult.tags == iMM.tags);
				});
			}
			//and add results
			if (self.MMs()[tmp_index]) {
				self.MMs()[tmp_index].result(new MM_result(iResult.results, new Date(iResult.ts)))
			};
		});
	};

	/*	Export MMs' results as JSON	*/
	self.export_MMs_results_JSON = function() {
		var tmp = [];
		self.MMs().forEach(iMM=>{
			if (iMM.visible()) {
				var tmp_res = iMM.result().getObjectValues(false);
				tmp_res.tags = iMM.tags;
				tmp_res.hash = iMM.hash;
				tmp_res.r = iMM.r();
				tmp.push(tmp_res);
			}
		});
		var tmp_json = JSON.stringify(tmp);
		console.log(tmp_json); //this can help with large results where saveToFile methods fails
		//actually save
		utils.saveToFile("results_export_openCQA.json", tmp_json);
	}

	/*	create authentication string and request templates	*/
	self.login = function() {
		config.authString = btoa($("#repoUser").val() + ":" + $("#repoPwd").val());
		self.got_connected(const_user_label);
		vm.get_templates();
	}

	/*	Requests paths occuring in dataset from server-app and adds them to self.paths	*/
	self.get_paths = async function() {
		self.paths = [];
		self.paths_infos = {};
		self.paths_info("Archetype-paths in retrieved dataset: 0");
		client.set_state("Querying repository and deriving archetype-paths...");
		try {
			var res = await $.ajax({
				async: true,
				type: "POST",
				contentType:"application/json",
				data: JSON.stringify({
						aql: vm.aql(),
						connection_id: client.connection_id,
						max_pagesize: parseInt(vm.max_pagesize())
				}),
				url: config.server_app.protocol+config.server_app.address+":"+config.server_app.port + "/query",
				beforeSend: function (xhr) {
					xhr.setRequestHeader ("Authorization", "Basic " + config.authString);
				}
			});
			//set GUI to wait for results
			self.retrieving_data_completed(false);
			return true;
		} catch (error) {
			client.set_state();
			console.error(error);
			client.show_hint("Problem processing the query!", 5000);
    }
	}

	/*	Requests a dataset row from server-app for given condition	*/
	self.get_result_row = async function(condition = 0) {
		try {
			var res = await $.ajax({
				async: true,
				type: "POST",
				contentType:"application/json",
				data: JSON.stringify({
						condition: condition
				}),
				url: config.server_app.protocol+config.server_app.address+":"+config.server_app.port + "/result_row",
				beforeSend: function (xhr) {
					xhr.setRequestHeader ("Authorization", "Basic " + config.authString);
				}
			});
			console.log(res);
			return res;
		} catch (error) {
			console.error(error);
    }
	}

	/*	Adds MMs grouping data in this dimension for all visible MMs	*/
	self.add_dimension_to_MMs = function(sender, event) {
		var MMs_to_add = [];
		//add MMs grouping data in this dimension for all visible MMs
		self.MMs().forEach((aMM, index) => {
			try {
				if (aMM.visible()) {
					var tmp_MM = aMM.clone();
					//add dimension to tags
					tmp_MM.tags = tmp_MM.tags + ',' + sender.name;
					//combine domain paths from dimension and original MM (tricky part is setting the right item values and resolving duplicates)
					if ((sender.domainPaths) && (sender.domainPaths != "")) {
						var iDimDomainPaths = JSON.parse(sender.domainPaths);
						var index=tmp_MM.domainPaths.length; //start counting from preexisting domainPaths
						var dim_rule = sender.rule;
						iDimDomainPaths.forEach(function(dP) {
							//catch dublicate domainPaths, e.g. item and item1 are the same path
							var duplicate = tmp_MM.domainPaths.find(function(tmpMMdP) {
								//only matching domainPaths is not enough, include pathExtension.asTextToAppend for both
								var dP_path_extension = "";
								if ((dP.pathExtension) && (dP.pathExtension.asTextToAppend)) {
									dP_path_extension = dP.pathExtension.asTextToAppend;
								}
								var tmpMMdP_path_extension = "";
								if ((tmpMMdP.pathExtension) && (tmpMMdP.pathExtension.asTextToAppend)) {
									tmpMMdP_path_extension = tmpMMdP.pathExtension.asTextToAppend;
								}
								return dP.domainPath + dP_path_extension  == tmpMMdP.domainPath + tmpMMdP_path_extension;
							});
							//need to set the right item in dimensions rule - e.g. grouping based on "item1" should become item2 if MM already uses two dPs
							var itemToReplace = dP.item;
							//found a duplicate entry - change dimension rule to use "item" already used in MM
							if (duplicate) {
								dP.item = duplicate.item;
							}
							//else new domainPath to add
							else {
								//add correct "item" value based on index
								dP.item = "item"+ index;
								//add new domainPath
								tmp_MM.domainPaths.push(dP);
								//count up index
								index++;
							}
							//set the right item in group function
							if (itemToReplace != dP.item) {
								dim_rule = dim_rule.replace(new RegExp(itemToReplace, "g"), dP.item);
							}
						});
						//if original MM has no grouping function - simply set dimension rule, if it already has one, tell R to concatenate their values
						tmp_MM.group = (tmp_MM.group == null) ? dim_rule : "sprintf('%s_%s', " + dim_rule + ", "+tmp_MM.group+")";
						//add new MM to self.MMs
						MMs_to_add.push(tmp_MM);
					}
				}
			} catch (error) {
				console.error(error);
			}
		});
		self.add_MMs(MMs_to_add, 'add_dimension_to_MMs('+ sender.name +')');
	}

	/*	Adds a dimension to dimension list	*/
	self.add_dimension_to_dimension_list = function(iName, iRule, iDomainPaths) {
		self.dimensions.push({name: iName, rule: iRule, domainPaths: iDomainPaths});
	}

	/*	Removes CIM (after asking for confirmation)	*/
	self.deleteDimension = function(sender, event) {
		if (confirm("Press OK to remove dimension? (" + sender.name + ")")) {
			self.dimensions.remove(sender);
		}
	}

	/*	set dimensions of interest, e.g. shall the characterization value, e.g. mean compliance to plausibility rule be returned for whole
	- dataset,
	- per site,
	- per participation
	- per patient,
	- per time interval (year, quarter, month, week, day, day of the week, before/after an intervention),
	- or sth. else?	*/
	self.setDimensions = function() {
		self.dimensions.removeAll();
		//per site
		self.add_dimension_to_dimension_list("per_site", "item1", JSON.stringify([{domainPath: "dataset-row/composition/context/health_care_facility/name", datatype:"string", pathExtension: null, item: "item1"}], null, 3));
		//composer - person primarily responsible for the content of the composition
		self.add_dimension_to_dimension_list("per_composer", "item1", JSON.stringify([{domainPath: "dataset-row/composition/composer/name", datatype:"string", pathExtension: null, item: "item1"}], null, 3));
		//per patient
		self.add_dimension_to_dimension_list("per_patient", "item1", JSON.stringify([{domainPath: "dataset-row/ehr/ehr_id/value", datatype:"string", pathExtension: null, item: "item1"}], null, 3));
		//per time interval (year)
		self.add_dimension_to_dimension_list("per_year", "item1", JSON.stringify([{domainPath: "dataset-row/composition/context/start_time/value", pathExtension: {asYear: true, asTextToAppend: ".asYear"}, datatype:"numeric", item: "item1"}], null, 3));
		//per time interval (quarter)
		self.add_dimension_to_dimension_list("per_quarter", "item1", JSON.stringify([{domainPath: "dataset-row/composition/context/start_time/value", pathExtension: {asQuarter: true, asTextToAppend: ".asQuarter"}, datatype:"string", item: "item1"}], null, 3));
		//per time interval (month)
		self.add_dimension_to_dimension_list("per_month", "item1", JSON.stringify([{domainPath: "dataset-row/composition/context/start_time/value", pathExtension: {asMonth: true, asTextToAppend: ".asMonth"}, datatype:"string", item: "item1"}], null, 3));
		//per time interval (week)
		self.add_dimension_to_dimension_list("per_week", "item1", JSON.stringify([{domainPath: "dataset-row/composition/context/start_time/value", pathExtension: {asWeek: true, asTextToAppend: ".asWeek"}, datatype:"string", item: "item1"}], null, 3));
		//per time interval (day)
		self.add_dimension_to_dimension_list("per_day", "item1", JSON.stringify([{domainPath: "dataset-row/composition/context/start_time/value", pathExtension: {asDay: true, asTextToAppend: ".asDay"}, datatype:"string", item: "item1"}], null, 3));
		//per time interval (day of the week)
		self.add_dimension_to_dimension_list("per_day_of_week", "item1", JSON.stringify([{domainPath: "dataset-row/composition/context/start_time/value", pathExtension: {asDayOfWeek: true, asTextToAppend: ".asDayOfWeek"}, datatype:"string", item: "item1"}], null, 3));
		//per time interval before or after a date of interest, e.g. before or after an intervention
		self.add_dimension_to_dimension_list("per_time_interval", "item1<0", JSON.stringify([{domainPath: "dataset-row/composition/context/start_time/value", pathExtension: {asDaysFrom: "2021-02-15", asTextToAppend: ".asDaysFrom(2021-02-15)"}, datatype:"numeric", item: "item1"}], null, 3));
	}();

	/*	use RM-type infos to derive more information on types of different paths that may occur in the data (on basis of RM definitions)	*/
	self.expand_paths_infos_data_types = function() {
		for (var key in self.paths_infos)  {
				var t = self.paths_infos[key].datatype;
				//can't do much for now, just inform developer ;-)
				if (t == "multiple") {
					console.warn("Multiple and contradicting types occured!");
				}
				else if (["HIER_OBJECT_ID", "TERMINOLOGY_ID", "GENERIC_ID"].includes(t)) {
					addType(key, "/value", "Id");//String in RM...but obviously i want to treat it a bit different since its an id
				}
				else if (t == "CODE_PHRASE") {
					//maybe TERMINOLOGY_ID stuff? //but this type should already be included in paths if relevant
					addType(key, "/code_string", "Code_string"); //String in RM...but i probably want to treat it a bit different since its a coded string
				}
				else if (t == "PARTY_IDENTIFIED") {
					addType(key, "/name", "String");
				}
				else if (t == "DV_BOOLEAN") {
					addType(key, "/value", "Boolean");
				}
				else if (t == "DV_CODED_TEXT") {
					//addType(key, "/defining_code", "CODE_PHRASE"); //this type should already be included in paths if relevant
					addType(key, "/value", "Code_string");
				}
				else if (t == "DV_COUNT") {
					addType(key, "/magnitude", "Integer");
				}
				else if (t == "DV_DATE") {
					addType(key, "/value", "ISO8601"); //String in RM...but obviously i want to treat it a bit different since its a ISO8601 date string
				}
				else if (t == "DV_DATE_TIME") {
					addType(key, "/value", "ISO8601"); //String in RM...but obviously i want to treat it a bit different since its a ISO8601 datetime string
				}
				else if (t == "DV_DURATION") {
					addType(key, "/value", "ISO8601_duration"); //String in RM...but obviously i want to treat it a bit different since its a ISO8601 duration string
				}
				else if (t == "DV_GENERAL_TIME_SPECIFICATION") {
					//addType(key, "/value", "DV_PARSABLE");//this type should already be included in paths if relevant
				}
				else if (t == "DV_IDENTIFIER") {
					addType(key, "/id", "Id");//String in RM...but obviously i want to treat it a bit different since its an id
					addType(key, "/issuer", "Id_issuer");
					addType(key, "/assigner", "Id_assigner");
					addType(key, "/type", "Id_type");
				}
				else if (t == "DV_ORDINAL") {
					addType(key, "/value", "Code_string");//it is an integer but it makes more sense to look at it like a code
					//addType(key, "/symbol", "DV_CODED_TEXT");//this type should already be included in paths if relevant
				}
				else if (t == "DV_PROPORTION") {
					addType(key, "/numerator:", "Real");
					addType(key, "/denominator:", "Real");
					addType(key, "/type:", "Code_string");//it is an integer but it makes more sense to look at it like a code
					addType(key, "/precision:", "Integer");
				}
				else if (t == "DV_QUANTITY") {
					addType(key, "/magnitude", "Real");
					addType(key, "/units", "Code_string"); //String in RM...but i want to treat it a bit different since its more like a coded string than a free text (i.e. we would not expect to many different values)
				}
				else if (t == "DV_TEXT") {
					addType(key, "/value", "String");
				}
				else if (t == "DV_TIME") {
					addType(key, "/value", "ISO8601"); //String in RM...but obviously i want to treat it a bit different since its a ISO8601 time string
				}
				else if (t == "DV_URI") {
					addType(key, "/value", "String");
				}
				//catch the ones i see nothing to do for now
				else if (["COMPOSITION","ELEMENT", "CLUSTER", "ITEM_TREE", "OBSERVATION", "HISTORY", "POINT_EVENT", "PARTY_SELF", "PARTICIPATION", "PARTY_REF", "DV_EHR_URI", "DV_GENERAL_TIME_SPECIFICATION", "DV_INTERVAL", "DV_MULTIMEDIA", "DV_PARSABLE", "DV_PERIODIC_TIME_SPECIFICATION", "DV_TIME_SPECIFICATION", "DV_SCALE", "DV_STATE", "DV_TEMPORAL", "PROPORTION_KIND", "REFERENCE_RANGE", "TERM_MAPPING", "OBJECT_VERSION_ID", "ARCHETYPED", "ARCHETYPE_ID", "TEMPLATE_ID", "FEEDER_AUDIT", "FEEDER_AUDIT_DETAILS", "EVENT_CONTEXT", "SECTION", "EVALUATION", "ACTION", "ISM_TRANSITION", true, "String","Code_string","Id_issuer","Id_assigner","Id_type","Id","Boolean","Integer","Real","ISO8601","ISO8601_duration","RM_type","AQL_path"].includes(t)) {}
				//warn if there is an unsupported type
				else {
					console.warn("Unsupported type occured: "+t+" ("+key+")");
				}
			}

			// TODO 5 SDV-KB if feeder audit holds original message/data and one MM that derives from another feeder audit entry (or some other source) the necessary backtransformation-rules than a MM can simply check item0 against item1 and we can create tables indicating if SDV is ok or not and also produce summary statistics about this - in combination with results coloring and show-me-this-data-row functionality this would probably be even really helpful
			//TODO 2.1 - Example KB demonstrating simple good looking exports of results by making use of rmarkdown/pandoc/"otherstuff like that", e.g. for some regular report - maybe built up on MeDIC dashboard.
			//TODO 2.0 dataquieR example as part of projektathon may be a good thing to demonstrate

			//add type for some fixed common paths
			var fixed_paths = [
				{path: utils.placeholders.const_dataset_row, type: 'dataset_row'},
				{path: '/id/value', type: 'Id'},
				{path: '/id/issuer', type: 'Id_issuer'},
				{path: '/id/assigner', type: 'Id_assigner'},
				{path: '/id/type', type: 'Id_type'},
				{path: '/uid/value', type: 'Id'},
				{path: '/code_string', type: 'Code_string'},
				{path: '/time/value', type: 'ISO8601'},
				{path: 'ehr_id/value', type: 'Id'},
				{path: 'ehr_status/subject/external_ref/namespace', type: 'Code_string'},
				{path: '/archetype_details/template_id/value', type: 'String'},
				{path: '/composer/name', type: 'String'},
				{path: '/category/value', type: 'Code_string'},
				{path: '/context/health_care_facility/name', type: 'String'},
				{path: '/context/location', type: 'Code_string'},
				{path: '/context/start_time/value', type: 'ISO8601'},
				{path: '/context/setting/value', type: 'Code_string'},
				{path: '/system_id', type: 'Id'},
				{path: '/audit/time_committed/value', type: 'ISO8601'},
				{path: '/data/origin/value', type: 'ISO8601'},
				{path: '/provider/name', type: 'Code_string'}
			];
			fixed_paths.forEach(fixed_path => {
				var matching_path = false;
				matching_path = self.paths.find(path => {
					return (path.indexOf(fixed_path.path)>-1);
				})
				if (matching_path) {
					//console.log("Added fixed datatype for: " + matching_path);
					self.paths_infos[matching_path].datatype = fixed_path.type;
				}
			});

		function addType(classPath, suffix, type) {
			var path = openEHR.get_parent_path(classPath) + suffix;
			//only if this path is occuring in the data - add its derived type to paths_infos
			if (self.paths.includes(path)) {
				self.paths_infos[path].datatype = type;
			}
			//fix entries for datatype if it is first time classPath's value is used
			if (self.paths_infos[classPath].datatype == t) {
				self.paths_infos[classPath].datatype = "RM_type";
				if (self.paths_infos[openEHR.get_parent_path(classPath)]) {
					self.paths_infos[openEHR.get_parent_path(classPath)].datatype = t;
				}
			}
		}
	}

	/*
		- if there is no data at all for a path in AQL-query, there should at least be a measure like count = 0
		- simple editable mapping from AQL-path to some kind of display name
	*/
	self.expand_paths_infos_with_AQL = function() {
		setTimeout(()=>{
			try {
				var m = new MM("",[]);
				var aql_paths = openEHR.AQL(self.aql()).get_AQL_paths();
				//catch if some AQL-path is not contained in paths_infos
				aql_paths.forEach((aql_path) => {
					var matched_path = self.paths.find((item) => {
						return (item.indexOf(utils.placeholders.const_dataset_row + aql_path.path.toLowerCase())>-1);
					});
					if (!matched_path) {
						self.paths_infos[utils.placeholders.const_dataset_row + aql_path.path.toLowerCase()] = {datatype: "AQL_path"};
						self.paths.push(utils.placeholders.const_dataset_row + aql_path.path.toLowerCase());
					}
				});
				//add some proposal for an alias to paths_info
				for (var path in self.paths_infos)  {
					self.paths_infos[path].alias = m.derive_alias_for_path_from_aql(path, aql_paths);
					var last_part = path.match(/\w+$/);
					if (last_part) {
						self.paths_infos[path].alias += "_" + last_part;
					}
				}
			} catch (error) {
				console.error(error);
			}
		},100);//if we don't set a short timeout possible displayed hints will be closed by process paths
	}

	/*	Clears context for this user from server-side and paths from client side	*/
	self.clear_data = async function() {
		$("#AQL_elements")[0].style.display = "none";
		self.paths = [];
		if (!config.authString) {
			client.show_hint("Missing information for authorization.", 5000);
			return;
		} //catch if no authorization is set

		var res;
		try {
			res = await $.ajax({
				async: true,
				type: "GET",
				contentType:"application/json",
				url: config.server_app.protocol+config.server_app.address+":"+config.server_app.port + "/removeContext",
				beforeSend: function (xhr) {
					xhr.setRequestHeader ("Authorization", "Basic " + config.authString);
				}
			});
			if (typeof res != "boolean") {
				client.show_hint("Clearing data on server side failed.", 5000);
				console.error(res);
			}
			else {
				client.show_hint("Clearing data on server side was successfull.", 3000);
			}
		} catch (error) {
			client.show_hint("Clearing data on server side failed.", 5000);
			console.error(error);
		}
	}

	/*	Requests templates available via openEHR-REST-API	*/
	self.get_templates = async function() {
		if (!config.authString) {
			client.show_hint("Missing information for authorization at data repository.", 5000);
			$("#credentials_elements")[0].style.display = "block";
			return;
		} //catch if no authorization is set

		var templates;
		try {
			templates = await $.ajax({
				async: true,
				type: "GET",
				contentType:"application/json",
				url: config.server_app.protocol+config.server_app.address+":"+config.server_app.port + "/templates",
				beforeSend: function (xhr) {
					xhr.setRequestHeader ("Authorization", "Basic " + config.authString);
				}
			});
			if (typeof templates == "string") {
				if (templates.indexOf("Code: 401")>-1) {
					client.show_hint("Authorization at data repository failed. Please check username and password.", 5000);
				}
				console.error(templates);
			}
			else {
				//indicate that credentials work for repository
				self.got_connected($("#repoUser").val());
				$("#credentials_elements")[0].style.display = "none";
				//add results to list
				self.CIMs.removeAll();
				templates.forEach(template => {
					self.CIMs.push(new CIM("platform", null, template.template_id, template.created_timestamp));
				});
				return self.CIMs().length;
			}
		} catch (error) {
			console.error(error);
    }
	}

	/*	Adds a CIM which was dragged and dropped into the GUI-Element displaying self.CIMs - so MMs can be derived from this CIM	*/
	self.dropped_kb = function(iString, iFilename) {
		if (iFilename.toLowerCase().indexOf(".json") > -1) {
			self.kbMMs.push(utils.parse_knowledge_base_file(iFilename, iString));
			self.kbMMs.sort(function (left, right) { return left.order == right.order ? 0 : (left.order < right.order ? -1 : 1) });
		}
		else {
			try {
				var aCIM = new CIM(iFilename, iString.replace("\uFEFF", "").replace("\n","").replace("\r",""), true);
				self.CIMs.push(aCIM);
			}
			catch (error) {
				console.error(error);
			}
		}
  };

	/*	When a CIM was clicked switch state of selection	*/
	self.clicked_CIM = async function(sender, event) {
		//mark as already used
		if(event.target.parentNode.tagName == "TR") {
			event.target.parentNode.className = "selected";
		} else if(event.target.parentNode.parentNode.tagName=="TR") {
			event.target.parentNode.parentNode.className = "selected";
		}
		if (!sender.xml) { //returns if definition needs to be requested
			var template;
			try {
				template = await $.ajax({
					async: true,
					type: "GET",
					contentType:"application/xml",
					url: config.server_app.protocol+config.server_app.address+":"+config.server_app.port + "/template/" + sender.id,
					beforeSend: function (xhr) {
						xhr.setRequestHeader ("Authorization", "Basic " + config.authString);
					}
				});
				//add results to list
				sender.xml = template;
				var aCIM = await sender.processCIM();
				if (aCIM) {
					self.add_MMs(aCIM.MMs(), aCIM.id, aCIM.atCodes);
					return "Retrieved and parsed successfully.";
				}
			} catch (error) {
				console.error(error);
				return "Retrieving and/or parsing failed.";
			}
		}
		else {
			self.add_MMs(sender.MMs(), sender.id, sender.atCodes);
		}
	}

	/*	Removes CIM (after asking for confirmation)	*/
	self.deleteCIM = function(sender, event) {
		if (confirm("Press OK to remove CIM " + sender.id + "?")) {
			self.CIMs.remove(sender);
		}
	}

	/*	invokes MM execution for all currently visible MMs	*/
	self.executeMMs = function() {
		var d = new Date();
		self.MMs().forEach(function(iMM, index) {
			if (iMM.visible()) {
				iMM.execute(self.MMs(), d);
			}
		});
	}

	/*	invokes MM execution for one MM	*/
	self.executeMM = async function(sender, event) {
		sender.execute(self.MMs(), new Date());
	}

	/*	Process incoming paths from dataset	*/
	self.process_paths = function(new_paths) {
		for (var key in new_paths)  {
			//if path has a value, this means its RM-type is specified in the data. Save it.
			if ((self.paths_infos[key]) &&
					(self.paths_infos[key].datatype) &&
					(self.paths_infos[key].datatype != new_paths[key])) {
				self.paths_infos[key] = "multiple";
			}
			else {
				self.paths_infos[key] = {datatype: new_paths[key]};
			}
			//store the path itself
			self.paths.push(key);
		}
		self.paths.sort(function (left, right) { return left.path == right.path ? 0 : (left.path < right.path ? -1 : 1) });
		self.expand_paths_infos_data_types();
		self.expand_paths_infos_with_AQL();
		client.set_state();
		self.paths_info("Archetype-paths in retrieved dataset: " + self.paths.length);
	}

	/*	handling of notification from server, that all data was retrieved from repository	*/
	self.handle_receiving_data_completed = function(response) {
		client.set_state();
		if (typeof response != "boolean") {
			console.error(response);
			client.show_hint("Invalid query!", 5000);
		}
		self.retrieving_data_completed(true);
	}

	/*	process server send event with result data from MM execution	*/
	self.process_SSE = function(event) {
		//catch send events from unexpected source (safety issue)
		if (event.origin != config.server_app.protocol+config.server_app.address+':'+config.server_app.port) {
				console.error("Got SSE from unexpected source! Not accepting this. This is a safety feature killing your work.");
				return;
		}
		//connection would be closed after 2 minutes, so server sends a keep_alive message each ~90 seconds
		if (event.data == "just_keep_me_alive") {
			return;
		}
		//connection_id to send with MM execution, so server-app knows where to send results
		if (client.connection_id == false) {
			client.connection_id = event.data;
			return;
		}
		var result = JSON.parse(event.data);
		//got message telling client that retrieving data from repository with query was completed
		if (result.retrieving_data_completed) {
			vm.handle_receiving_data_completed(result.retrieving_data_completed);
		}
		//messages holding dataPaths
		else if (!result.MM_hash) {
			self.process_paths(result.paths);
		}
		//messages holding MM-results
		else {
			try {
				self.running_MMs(self.running_MMs()-1);
				//...since protocols differ there are some cases where fast MM's results arrive before servers confirmation that this MM was executed is processed (the confirmation is used for the hash)
				setTimeout(function() {
					try {
						self.MMs().find(function(iMM) {return iMM.hash == result.MM_hash}).add_result(result);
					} catch (error) {
						setTimeout(function() {
							try {
								self.MMs().find(function(iMM) {return iMM.hash == result.MM_hash}).add_result(result);
							} catch (error) {
								console.error(result);
							 	console.error(error);
								console.error("Most probably for some reason MM for MMs Hash could not be found.");
							}
						}, 5000);
					}
				}, 100);
			} catch (error) {
				console.error(result);
			 	console.error(error);
			}
		}
	}

	// TODO 5 display data of dataset row specified by filter condition (or at least row number)

	/*	shows/hides the results table for a MM	*/
	self.show_hide_MM_results_table = function(sender, event) {
		sender.show_results_table(!sender.show_results_table());
	}

	/*	sorts the results table for a MM	*/
	self.sort_MM_results_table = function(sender, event) {
		if (sender.result()) {
			//set sorting functino for strings or numeric values depending on column
			var key = event.target.id;
			var sort_function = (['value','size'].includes(key)) ?
				(a,b) => {
					return a[key]-b[key];
				} :
				(a,b) => {
					if (a[key] < b[key]) {
					  return -1;
					}
					if (a[key] > b[key]) {
					  return 1;
					}
					return 0;
				};
			//reverse order with each click
			event.target.reverse = (event.target.reverse == 1) ? -1 : 1;
			var directed_sort_function = (a,b) => {return event.target.reverse * sort_function(a,b)}
			//do it
			sender.result().results.sort(directed_sort_function);
			sender.result.valueHasMutated();
		}
	};

	/*	copys the results table for a MM to the clipboard	*/
	self.copy_MM_results_table = function(sender, event) {
		if (sender.result()) {
			try{
				sender.show_results_table(true);
				window.getSelection().removeAllRanges(); //important to work on chrome
		    var range = document.createRange();
		    range.selectNode(event.target.parentNode.parentNode.parentNode);
		    window.getSelection().addRange(range);
		    document.execCommand('copy');
			}
			catch(error) {
				console.error(error);
			}
		}
	};

	/*	simple filtering for MMs.
		- no filter (input.value is empty string) - show all MMs
		- number based, e.g. 472 - show MM 472
		- tag based, e.g. count - show MMs with tag "count"
		- text based, e.g. (iMM.tags.indexOf("row_count")>-1)
	*/
	self.filterMMs = function(sender, event) {
		//set self.mmFilter() based on other filters
		var filterConditions = [];
		if (self.nrFilter()!="") {
			//number based, e.g. 472 - show MM 472; >400 show MMs beginning from 401
			var tmpTags = self.nrFilter().split(",");
			var nrConditions = [];
			tmpTags.forEach(function(itmpTag) {
				nrConditions.push(('(i=='+itmpTag+')').replace("==>",">").replace("==<","<"));
			});
			//now create filter expression
			filterConditions.push('(' + nrConditions.join(' || ') + ')');
		}
		if (self.tagFilter()!="") {
			//tag based, e.g. count - show MMs with tag "count"
			var tmpTags = self.tagFilter().split(",");
			tmpTags.forEach(function(itmpTag) {
				//catch negation of tag expression by !
				var negation = false;
				if (itmpTag[0]=="!") {
					itmpTag = itmpTag.substr(1);
					negation = true;
				}
				//catch if only substring of tag is used to match, e.g. "_per_"
				if (itmpTag.indexOf('"')>-1) {
					negation = (negation) ? "==" : '>';
					filterConditions.push('(iMM.tags.indexOf('+itmpTag+')'+ negation +'-1)');
				}
				//just a simple tag filter
				else {
					negation = (negation) ? "!" : '';
					filterConditions.push('('+negation+'tags.includes("'+itmpTag+'"))');
				}
			});
		}
		if (self.executionFilter()!="") {
			//simple text based filter on execution date/time
			var tmpTags = self.executionFilter().split(",");
			tmpTags.forEach(function(itmpTag) {
				filterConditions.push('((iMM.executionTime().display) && (iMM.executionTime().display.indexOf("'+itmpTag+'")>-1))');
			});
		}
		if (self.domainPathFilter()!="") {
			//text based on domainPath, e.g. '(iMM.domainPaths[0].domainPath.indexOf("diagnosis")>-1)'
			var tmpTags = self.domainPathFilter().split(",");
			tmpTags.forEach(function(itmpTag) {
				filterConditions.push('(iMM.single_domain_path_to_text(iMM.domainPaths[0]).indexOf("'+itmpTag+'")>-1)');
			});
		}
		//set self.mmFilter()
		if (filterConditions.length>0) {
			self.mmFilter(filterConditions.join(" && "));
		}
		//resolve how to filter
		var tmpFilterFunction;
		//no filter - show all MMs
		if (self.mmFilter()=="") {
			self.visibleMMs("No filter applied.");
			tmpFilterFunction = function(iMM) {return true;}
		}
		//simply eval expression in mmFilter()
		else {
			tmpFilterFunction = function(iMM,i) {
				var tags = iMM.tags.split(",");
				return eval(self.mmFilter());
			}
		}
		//now iterate through MMs and apply filter
		var visibleMMs = 0;
		self.MMs().forEach((iMM, i) => {
			iMM.visible(tmpFilterFunction(iMM,i));
			if (iMM.visible()) {visibleMMs++;}
		});
		self.visibleMMs(visibleMMs);
	}

	/*	clears all MM filters	*/
	self.clear_MM_filter = function() {
		//clear all filters
		self.nrFilter("");
		self.tagFilter("");
		self.executionFilter("");
		self.domainPathFilter("");
		self.mmFilter("");
		//update visible MMs with all filters removed
		self.filterMMs();
	}

}
