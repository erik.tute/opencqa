(function(exports){
	exports.server_app = {
			port: 3000,
			address: 'localhost',
			protocol: 'http://'
	};
	exports.r = {
		path: 'C:\\Program Files\\R\\R-3.3.0\\bin\\R'
	};
	exports.openEHRServer = {
		REST_API_Address: 'xxx.xxx.xx.xx',
		REST_API_Port: '8081',
		protocol: 'http://',
		REST_API_TYPE: 2
	};
	exports.openEHRServer_dummy_data_example = {
		DATA_FILE: 'C:\\Users\\ErikT\\Desktop\\openCQA\\Server app\\client\\public\\example_dummy_data.json',
		REST_API_TYPE: 3,
	};
	exports.display_options = {
		show_results_by_default_threshold: 50
	};
})(typeof exports === 'undefined'? this['config']={}: exports);
