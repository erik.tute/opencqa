(function(exports){

	exports.placeholders = {
		const_dataset_row: "dataset-row/",
		const_other_data_input: "other_data_input:"
	};

	/*	logs a given message in console - max limits execution of this method, e.g. for dev cases where you want to log a value*/
	exports.l = (function (msg, max = 10) {
		var counter = 0;
		return function (msg, max = 10) {
			if (counter<max) console.log(msg);
			counter += 1;
		}
	})();

	/*	(node.js) reads data from specified file and returns it as string */
	exports.readFileAsText = function(iFilename, callback, absolute_path = false) {
		try {
			var fs = require('fs');
			var filename = (!absolute_path) ? __dirname + iFilename : iFilename; //allow for absolute paths
			fs.readFile( filename, function (err, data) {
				if ((err) && ((err).code) && (err.code === 'ENOENT')) {
					console.error('File not found: ' + iFilename);
					callback("",iFilename);
				} else if (err) {
					console.error(err);
					throw err;
				}
				else {
					callback(data.toString(),iFilename);
				}
			});
		}
		catch (err) {
			console.error(err);
		}
	};

	/*	(node.js) writes data in specified file */
	exports.writeToFile = function(iFilename, iString, overwrite=false) {
		var fs = require('fs');
		var writingFunction = (overwrite) ? fs.writeFileSync: fs.appendFileSync; //append to file or write new file depending on given value in overwrite
		try {
			writingFunction(__dirname + iFilename, iString);
		}	catch	(err)	{
			throw err;
		}
	};

	/*	(node.js) Removes all files from given folder	*/
	exports.clearDir = function(iDir, iSkiplist = false) {
		const fs = require('fs');

		fs.readdir(iDir, (err, files) => {
		  if (err) throw err;

		  for (const file of files) {
				if ((iSkiplist) && (iSkiplist.includes(file))) {
					continue;
				}
				fs.unlink(iDir + file, err => {
				  if (err) throw err;
				});
		  }
		});
	}

	/*	generates a quick simple hash (not applicable to security stuff!) */
	exports.generate_hash = function(iString) {
		if (!iString) return 0;
    var hash = 0, i, chr;
    for (i = 0; i < iString.length; i++) {
      chr   = iString.charCodeAt(i);
      hash  = ((hash << 5) - hash) + chr;
      hash |= 0; // Convert to 32bit integer
    }
    return hash;
  }

	/*	generates a random ID (Chars and Numbers) of specified length*/
	exports.generateId = function(length, numbersOnly = false) {
		var id = "";
		var alphabet = (numbersOnly) ? "123456789" : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		for (var i = 0; i < length; i++) {
			id+= alphabet[Math.floor(Math.random() * alphabet.length)];
		}
		return id;
	};

	/*	Returns the ISO week of the date.	*/
	exports.getWeek = function(date_value) {
		// Original script from: https://weeknumber.net/how-to/javascript
	  var date = new Date(date_value.getTime());
	  date.setHours(0, 0, 0, 0);
	  // Thursday in current week decides the year.
	  date.setDate(date.getDate() + 3 - (date.getDay() + 6) % 7);
	  // January 4 is always in week 1.
	  var week1 = new Date(date.getFullYear(), 0, 4);
	  // Adjust to Thursday in week 1 and count number of weeks from date to week1.
	  return 1 + Math.round(((date.getTime() - week1.getTime()) / 86400000
	                        - 3 + (week1.getDay() + 6) % 7) / 7);
	}

	/*	generates a contextualized MM (R-script) from given input R-script-template and parameters (domainPaths, check function, grouping function, characterization)	*/
	exports.contextualizeMMTemplate = function(iTemplate, iParameters, iPublicDirectory) {
		iParameters_copy = JSON.parse(JSON.stringify(iParameters));
		//add domain paths and datatype information
		iTemplate = iTemplate.replace('#domainPaths_replace', iParameters_copy.displayedDomainPaths + '\n');

		iTemplate = setRFunctionFromCheck(iTemplate, iParameters_copy.check, 'check');
		iTemplate = setRFunctionFromCheck(iTemplate, iParameters_copy.group, 'group');

		//add begin and end instructions for plots in characterization
		var filepath = iPublicDirectory.replace(/\\/g, "\/") +"/plots/";
		iParameters_copy.characterization = iParameters_copy.characterization
		.replace('#begin_plot',
		`fn = sample(100000:999999, 1)
		svg(sprintf("${filepath}%d.svg", fn))`)
		.replace('#begin_pdf_plot',
		`fn = sample(100000:999999, 1)
		pdf(sprintf("${filepath}%d.pdf", fn))`);
		iParameters_copy.characterization = iParameters_copy.characterization
		.replace('#end_plot', 'dev.off()\nreturn(fn)\n');
		iTemplate = iTemplate.replace( 'characterization <- NULL', 'characterization = ' + iParameters_copy.characterization);

		return iTemplate;

		function setRFunctionFromCheck(iTemplate, iCheck, iName) {
			if (!iCheck) { //if iCheck is not set, don't replace the dummy variable in template
				return iTemplate;
			}
			else {
				//item elements which are appended to mapply and its target function(s), e.g. item, item1, item2
				var items = iParameters_copy.domainPaths.map(function(dP) {
					return dP.item;
				}).join(', ');
				//create final check and replace it in template
				var tmp = iName + ' = function(' + items + ') { \n' + iCheck + '\n}';
				return iTemplate.replace(iName+' <- NULL', tmp).replace(/replace_with_items/g, items);
			}
		}
	}

	/*	removes	matchedPath and inputValues from all pathes in given domainPaths and sets check and group to null if string is empty*/
	exports.clean_domain_paths = function(JSON_containing_domainPaths, structure, pretty = false) {
		if (!JSON_containing_domainPaths) {return;}
		var obj = JSON.parse(JSON_containing_domainPaths);

		if (structure == "domainPaths") {
			cleanDomainPaths(obj);
		}
		else if (structure == "MM") {
			cleanDomainPaths(obj.domainPaths);
			if (obj.check=="") {obj.check = null;}
			if (obj.group=="") {obj.group = null;}
		}
		else if (structure == "MM_array") {
			obj.forEach((a_mm, i) => {
				cleanDomainPaths(a_mm.domainPaths);
				if (a_mm.check=="") {a_mm.check = null;}
				if (a_mm.group=="") {a_mm.group = null;}
			});
		}
		else if (structure == "KB") {
			obj.MMs.forEach((a_mm, i) => {
				cleanDomainPaths(a_mm.domainPaths);
				if (a_mm.check=="") {a_mm.check = null;}
				if (a_mm.group=="") {a_mm.group = null;}
			});
		}

		if (pretty) {
			return JSON.stringify(obj, null, 3);
		}
		else {
			return JSON.stringify(obj);
		}

		function cleanDomainPaths(domainPaths) {
			//iterate through domainPaths and clean them
			domainPaths.forEach((dp, i) => {
				delete dp.matchedPath;
				if (dp.pathExtension) {
					delete dp.pathExtension.inputValues;
				}
			});
		};
	}

	exports.parse_knowledge_base_file = function(filename, content_string) {
		try {
			var tmp_kb = JSON.parse(content_string);
			//catch older KBs which were only an array of MMs
			if (Array.isArray(tmp_kb)) {
				tmp_kb = {kb_tag: filename.replace("kb_","").replace(".json",""), MMs: tmp_kb}
			}
			else {
				tmp_kb.kb_tag = filename.replace("kb_","").replace(".json","");
			}
			tmp_kb.description = (tmp_kb.description) ? tmp_kb.description : null;
			tmp_kb.order = (tmp_kb.order) ? tmp_kb.order : 999999;
			return tmp_kb;
		}
		catch (error) {
			//console.log(error);
			return {MMs: [], kb_tag: "ERROR", description: "Parsing of KB "+filename+" failed!", order: 1};
		}
	}

	/*	(client side) downloads a string to a file	*/
	exports.saveToFile = function(filename, string) {
		var a = document.createElement('a');
		a.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(string));
		a.setAttribute('download', filename);

		if (document.createEvent) {
			var event = document.createEvent('MouseEvents');
			event.initEvent('click', true, true);
			a.dispatchEvent(event);
		}
		else {
			a.click();
		}
	}

	 /*  (client side) stops further event handling (bubbling up) */
	exports.stopEvent = function(event) {
		if (!event)
			var event = window.event;
		//IE
		event.cancelBubble = true;
		event.returnValue = false;
		//other
		if (event.stopPropagation)
			event.stopPropagation();
		if (event.preventDefault)
			event.preventDefault();
		return false;
	};

	/*
		(client side) Functionalities for dragging and dropping textfiles for further processing
		1. use exports.dnd.addEvents(DOM-element) to add needed EventListeners to DOM-element with drag and drop support
		2. replace exports.dnd.perFile(String) and exports.dnd.afterLastFile() with usefull functions to process droped files
	*/

	exports.dnd = exports.dnd || 	{};

	exports.dnd.addEvents = function(iElement) {
		iElement.addEventListener('drop', exports.dnd.onDropFile);
		iElement.addEventListener('dragover', exports.dnd.onDragOver);
	};

	exports.dnd.perFile = function(iString, iFilename) {
		console.log("exports.dnd.perFile: Replace this dummy function to process data for each file");
		console.log(iString);
	};

	exports.dnd.afterLastFile = function() {
		console.log("exports.dnd.afterLastFile: Finished reading last file. Replace this dummy function with sth. usefull.");
	};

	exports.dnd.onDragOver = function(event) {
		event = event ? event : window.event;
		exports.stopEvent(event);
		event.dataTransfer.dropEffect = 'copy';
	};

	exports.dnd.onDropFile = function(event) {
		var filesToParse = null, filesParsed = null;
		event = event ? event : window.event;
		exports.stopEvent(event);
		if (event.dataTransfer.files !== undefined) {
			filesToParse = event.dataTransfer.files.length;
			filesParsed = 0;
			for (var i = 0, le = event.dataTransfer.files.length; i < le; i++) {
				var f = event.dataTransfer.files[i];
				var reader = new FileReader();

				reader.onload = (function(filename) {
	      	return function(e) {
		        finishedReading(e.target.result, filename);
		      };
	    	})(f.name);

				reader.readAsBinaryString(f);
			}
		}

		function finishedReading(iTxt, iFilename) {
			filesParsed++;
			exports.dnd.perFile(iTxt, iFilename);
			if (filesParsed == filesToParse) {
				exports.dnd.afterLastFile();
			}
		}
	};

})(typeof exports === 'undefined'? this['utils']={}: exports);
