(function(exports){
	var http;

	//AQL handling class
	exports.AQL = function(i_query_string) {
		var self = {};
		//split AQL into its sections SELECT, variables (FROM, CONTAINS), WHERE, ORDER BY, OFFSET, LIMIT
		//https://specifications.openehr.org/releases/QUERY/latest/AQL.html#_reserved_words_and_characters
		//split for SELECT part
		var parts = i_query_string.replace(/ *--.*$/gm,"").replace(/\n\n+/g,"\n").trim().split(/(?:\s)+from(?:\s)+/i);
		//deal with DISTINCT
		self.distinct =  (/(\s)*distinct(\s)+/i.test(parts[0])) ? "DISTINCT " : "";
		self.select = parts[0].replace(/(\s)*select(\s)+/i,"").replace(/(\s)*distinct(\s)+/i,"");
		//deal with TOP structure in AQL
		self.top = "";
		if (/(?:\s)?top(?:\s)+/i.test(self.select)) {
			try {
				self.top = self.select.match(/(?:\s)?top(?:\s)\d+/i)[0];
				self.select = self.select.replace(self.top,"").trim();
				self.top = "TOP " + parseInt(self.top.replace(/top/i,"").trim()) + ' ';
			} catch (error) {
				console.error("Error while trying to parse TOP value.");
				console.error(error);
			}
		}
		//split select further into aql-paths
		self.select = self.select.split(/,(?![^\[]*\])/g);//split by all commas not in squared
		self.select = self.select.map(path => path.trim());
		parts = parts[1];
		//split for LIMIT part
		parts = parts.split(/(?:\s)limit(?:\s)+/i);
		self.fetch = (parts.length > 1) ? parseInt(parts[1]) : -1;
		parts = parts[0];
		//split for OFFSET part
		parts = parts.split(/(?:\s)offset(?:\s)+/i);
		self.offset = (parts.length > 1) ? parseInt(parts[1]) : -1;
		parts = parts[0];
		//split for ORDER BY part
		parts = parts.split(/(?:\s)order by(?:\s)+/i);
		self.order_by = (parts.length > 1) ? `
ORDER BY ` + parts[1] : "";
		parts = parts[0];
		//split for WHERE part
		parts = parts.split(/(?:\s)where(?:\s)+/i);
		self.where = (parts.length > 1) ? `
WHERE ` + parts[1] : "";
		parts = parts[0];
		//split for FROM part
		var parts_special = parts.split(/(?:\s)contains(?:\s)+/i);
		parts_special = parts_special.map(elem => elem.trim());
		self.from = `
FROM ` + parts_special[0];
		self.contains = "";
		if (parts_special.length>1) {
			parts_special.shift();
			self.contains = `
  CONTAINS ` + parts_special.join(`
  CONTAINS `);
		}
		//split for variables
		parts = parts.split(/(?:\s)contains(?:\s)|(?:\s)and|(?:\s)or/gi);
		self.variables = parts.map((elem) => {return elem.replace(/\(/g,"").replace(/\)/g,"").trim()});
		//catch TOP and OFFSET/LIMIT/FETCH in one query
		if (self.top != "") {
			if ((self.offset!=-1) || (self.fetch!=-1)) {
				console.warn("OFFSET and LIMIT/FETCH are not supported if TOP is used in AQL-query.");
			}
			self.offset = -1;
			self.fetch = -1;
		}

		/*	returns AQL-object as query string	*/
		self.as_query_string = function() {
			var paths_separator = `,
   `;
			return "SELECT " + self.top + self.distinct + self.select.join(paths_separator) + self.from + self.contains + self.where + self.order_by;
		}

		//just for backwards compatibility - old REST-API query endpoint does not support offset and fetch parameters
		self.as_query_string_with_offset_and_limit = function() {
			var offset_string = (self.offset > -1) ? `
OFFSET ` + self.offset : "";
			var limit_string = (self.fetch > -1) ? `
LIMIT ` + self.fetch : "";
			return self.as_query_string() + offset_string + limit_string;
		}

		/*	creates paths for each "item" in select statement of AQL 	*/
		self.get_AQL_paths = function() {
			var aql_paths = [];
			self.select.forEach((a_path, i) => {
					var var_to_replace = a_path.split('/')[0].trim(); //get variable name
					//now get value (archetype_id or class) to replace variable name with
					var matching_contains_statement = self.variables.find((current_contains_statement) => {
						var tmp_var = current_contains_statement.trim().split(" ")[1].replace(/(\[.*?\])/,"");
						return (tmp_var == var_to_replace);
					});
					var replace_with =  (matching_contains_statement.indexOf("[")==-1) ? matching_contains_statement.trim().split(" ")[0] : matching_contains_statement.match(/(\[.*?\])/)[0];
					a_path = a_path.replace(var_to_replace, replace_with);
					//now deal with the as part, e.g. a_wild_path as a_variable_name
		 			a_path =  a_path.split(/(?:\s)as(?:\s)+(?![^\[]*\])/i);
					var a_path_obj = {path: a_path[0].trim()};
					a_path_obj.as = (a_path.length > 1) ? a_path[1].trim() : false;
					aql_paths.push(a_path_obj);
			});
			return aql_paths;
		};

		/*	derives an AQL-query aiming to retrieve a small amount of data but many reference model types */
		self.derive_AQL_for_rm_types = function() {
			//simply creates AQL without ordering etc. limited to small amount of results and querying "parent paths" i.e. aPath/value instead of aPath/value/value to retrieve datatype without need to retrieve the big tree for millions of records when only leafnodes are of interest
			var aql = openEHR.AQL(self.as_query_string());
			aql.offset = 0;
			aql.fetch = 10;
			aql.order_by = "";
			aql.select = aql.select.map(a_path => {
				return openEHR.get_parent_path(a_path);
			});
			return aql;
		}

		return self;
	};

	/*	derives the parent path for an archetype-path. e.g.: [openEHR-..._signs.v1]/data[Event Series] -> [openEHR-..._signs.v1]/data	- if path has no parent the original path is returned */
	exports.get_parent_path = function(iPath) {
		//catch if path expression contains alias
		var parts = iPath.split(/(?:\s)as(?:\s)+(?![^\[]*\])/i);
		var alias = (parts.length>1) ? " AS " + parts[1] : "";
		iPath = parts[0];
		var p = iPath.lastIndexOf('/');
		var p2= iPath.lastIndexOf('[');
		if (p<p2) {
			p = p2;
		}
		//catch if there is no parent path to get -> return current path
		if ((p==-1) || (p==iPath.length-1)) {
			return iPath;
		}
		return iPath.substr(0, p) + alias;
	};

	//class handling interaction with openEHR REST_API
	exports.REST_API_ADAPTER = function(repository) {
		const placeholder_template_id = '<<template_id>>';
		const const_repository_sent_error = "Repository sent error.";
		if (repository.REST_API_TYPE==1) {
			self = new REST_API_ADAPTER_TYPE_1(repository);
		}
		else if (repository.REST_API_TYPE==2){
			self = new REST_API_ADAPTER_TYPE_2(repository);
		}
		else if (repository.REST_API_TYPE==3){
			self = new REST_API_ADAPTER_TYPE_3(repository);
		}
		else if (repository.REST_API_TYPE==4){
			self = new REST_API_ADAPTER_TYPE_4(repository);
		}
		if ((repository.REST_API_TYPE==1) || (repository.REST_API_TYPE==2)) {
			http = (repository.protocol.indexOf("https")>-1) ? require('https') : require('http');
		}

		/*	function to send request to an openEHR-REST-API*/
		self.REST_request = async function(iPath, iType, iAuth, iContentType, iBody) {
			const data = JSON.stringify(iBody);
			var options = {
				host: self.repository.REST_API_Address,
				port: self.repository.REST_API_Port,
				path: encodeURI(iPath),
				method: iType,
				headers: {
					'authorization': iAuth,
					'content-type': iContentType,
					'content-length': Buffer.byteLength(data),
					'Accept': iContentType
				}
			};

			return new Promise((resolve, reject) => {
		    const req = http.request(options, res => {
		      if (res.statusCode < 200 || res.statusCode >= 300) {
		        return reject(new Error(`Status Code: ${res.statusCode}`));
		      }
		      const data = [];
		      res.on('data', chunk => {
		        data.push(chunk);
		      });
		      res.on('end', () => resolve(Buffer.concat(data).toString()));
		    });

		    req.on('error', reject);

				req.write(data);
		    req.end();
			});
		}

		async function common_get_templates(authorization) {
			try {
				var result = await self.REST_request(
					self.templates_endpoint,
					"GET", authorization,
					'application/json',
					false
				);
				result = JSON.parse(result);
				result = self.preprocess_templates(result);
				return result;
			} catch (error) {
				console.error(error);
				return "(Detailed message on server) " + error.toString();
			}
		}

		async function common_get_template_opt(authorization, template_id) {
			try {
				var result = await self.REST_request(
					self.template_opt_endpoint.replace(placeholder_template_id, template_id),
					"GET",
					authorization,
					'application/xml',
					false
				);
				return result;
			} catch (error) {
				console.error(error);
				return error;
			}
		}

		/*	request data in pages	*/
		async function common_get_data_in_pages(con_id, auth, aql, index, pagesize, overall_limit, data_callback, completed_callback) {
			try {
				//adjust offset and limit
				if (aql.top != "") {
					aql.offset = -1;
					aql.fetch = -1;
					overall_limit = Math.min(overall_limit, aql.top);
				}
				else {
					aql.offset = index;
					var page_limit = pagesize;
					if ((overall_limit>-1) && (index + pagesize > overall_limit)) {page_limit = overall_limit-index;}
					aql.fetch = page_limit;
				}
				//retrieve data
				var result_set = await self.get_dataset(auth, aql);
				//catch if there is no more data to retrieve
				if (eval(self.const_resultset_empty_condition)) {
					completed_callback(con_id, true);
					return;
				}
				else if (result_set.indexOf(const_repository_sent_error)==0) {
					completed_callback(con_id, "(Detailed message on server) " + result_set);
					return;
				}
				//are we done or do we need to send next request?
				if ((overall_limit==-1) || (index+page_limit < overall_limit)) {
					common_get_data_in_pages(con_id, auth, aql, index+pagesize, pagesize, overall_limit, data_callback, completed_callback);
				}
				//we got all data up do specified limit
				else {
					completed_callback(con_id, true);
				}
				var data_array = self.parse_query_result_set(result_set, aql.get_AQL_paths());
				data_callback(con_id, auth, data_array, self.rm_type_regex);
			} catch (error) {
				console.error(error);
				completed_callback(con_id, error);
			}
		}

		self.get_dataset = async function(authorization, aql) {
			try {
				var result = await self.REST_request(
					self.query_endpoint,
					'POST',
					authorization,
					'application/json',
					self.create_query_request_body(aql)
				);
				return result;
			} catch (error) {
				console.error(error);
				return const_repository_sent_error + error.toString();
			}
		}

		/*	convert AQL-query result set into desired data structure	*/
		self.parse_query_result_set = function(result_set, aql_paths) {
			try {
				result_set = JSON.parse(result_set);
				return self.preprocess_query_resultset(result_set, aql_paths);
			}
			catch (error) {
				return error;
			}
		}

		//adapter class for Think/Better
		function REST_API_ADAPTER_TYPE_1(repository) {
			self = {};
			self.repository = repository;
			self.templates_endpoint = '/rest/v1/template';
			self.template_opt_endpoint = '/rest/v1/template/'+ placeholder_template_id + '/opt';
			self.query_endpoint = '/rest/v1/query';
			self.const_resultset_empty_condition = '(result_set == "")';
			self.preprocess_templates = function(templates) {
				try {
					templates = templates.templates;
					templates = templates.map(template => {
						return {template_id: template.templateId, created_timestamp: template.createdOn};
					});
					return templates;
				} catch (error) {
					console.error(error);
					return (error);
				}
			}
			self.create_query_request_body = function(aql) {
				return request_body = {aql: aql.as_query_string_with_offset_and_limit()};
			}
			self.preprocess_query_resultset = function(result_set, aql_paths) {
				try {
					var data_array = [];
					result_set.resultSet.forEach(row => {
						var data_row = {};
						for (var key in row) {
							//resolve alias (e.g. AQL: SELECT a[wild]/path AS alias FROM ... - key will be "alias", but path should be "a[wild]/path")
							var compare_key = key.toLowerCase();
							var resolved_as = aql_paths.find(function(aql_path) {
								if (!aql_path.as) {
									return false;
								}
								else {
									return aql_path.as.toLowerCase() == compare_key;
								}
							});
							if (resolved_as) {
								data_row[resolved_as.path] = row[key];
							}
							//resolve column numbers (i.e. no alias was given)
							else if (compare_key.match(/#[0-9]+/g)) { //insert paths from AQL within data-paths
								data_row[aql_paths[Number(compare_key.replace('#',''))].path] = row[key];
							}
							else {
								console.warn("Can't resolve key " + key + " in:");
								console.warn(row[key]);
							}
						}
						data_array.push(data_row);
					});
					return data_array;
				} catch (error) {
					console.error(error);
					return (error);
				}
			}
			//begin actual adapter interface
			self.rm_type_regex = /@class$/;
			self.get_templates = common_get_templates;
			self.get_template_opt = common_get_template_opt;
			self.get_data_in_pages = common_get_data_in_pages;
			//end actual adapter interface
			return self;
		}

		//adapter class for EHRbase
		function REST_API_ADAPTER_TYPE_2(repository) {
			self = {};
			self.repository = repository;
			self.templates_endpoint = '/ehrbase/rest/openehr/v1/definition/template/adl1.4';
			self.template_opt_endpoint = '/ehrbase/rest/openehr/v1/definition/template/adl1.4/' + placeholder_template_id;
			self.query_endpoint = '/ehrbase/rest/openehr/v1/query/aql';
			self.const_resultset_empty_condition = '(result_set.indexOf(\'"rows":[]}\') > -1)';
			self.preprocess_templates = function(templates) {
				return templates;
			}
			self.create_query_request_body = function(aql) {
				var request_body = {};
				request_body.q = aql.as_query_string();
				request_body["Content-Type"] = 'application/json';
				if (aql.offset>-1) {request_body.offset = aql.offset;}
				if (aql.fetch>-1) {request_body.fetch = aql.fetch;}
				return request_body;
			}
			self.preprocess_query_resultset = function(result_set, aql_paths) {
				var data_array = [];
				result_set.rows.forEach(row => {
					var data_row = {};
					row.forEach((rows_object_for_column, col_index) => {
						data_row[aql_paths[col_index].path] = rows_object_for_column;
					});
					data_array.push(data_row);
				});
				return data_array;
			}
			//begin actual adapter interface
			self.rm_type_regex = /_type$/;
			self.get_templates = common_get_templates;
			self.get_template_opt = common_get_template_opt;
			self.get_data_in_pages = common_get_data_in_pages;
			//end actual adapter interface
			return self;
		}

		//TODO 1 REST_API_ADAPTER_TYPE_3 file selection like in ADAPTER TYPE 4
		//dummy adapter class e.g. for tests
		function REST_API_ADAPTER_TYPE_3(repository) {
			self = {};
			self.repository = repository;
			//begin actual adapter interface
			self.rm_type_regex = /_type$/;
			self.get_templates = function() {return [];}
			self.get_template_opt = function() {return "";}
			self.get_data_in_pages = function(con_id, auth, aql, index, pagesize, overall_limit, data_callback, completed_callback) {
				require('./utils').readFileAsText(self.repository.DATA_FILE, (json_string) => {
					var static_data = JSON.parse(json_string);
					if (static_data[aql.as_query_string()]) {
						completed_callback(con_id, true);
						data_callback(con_id, auth, static_data[aql.as_query_string()], self.rm_type_regex);
					}
					else {
						console.error("File " + repository.DATA_FILE + " has no data for query: "+ aql.as_query_string());
						completed_callback(con_id, "File " + repository.DATA_FILE + " has no data for query: "+ aql.as_query_string());
					}
				}, true);
			}
			//end actual adapter interface

			return self;
		}

		//dummy adapter class e.g. for tests
		function REST_API_ADAPTER_TYPE_4(repository) {
			self = {};
			self.repository = repository;
			//begin actual adapter interface
			self.filebased = true;
			self.rm_type_regex = /_type$/;
			self.get_templates = function() {return [];}
			self.get_template_opt = function() {return "";}
			self.get_data = function(con_id, auth, filename_seperator, data_callback, completed_callback) {
				var filename_seperator = filename_seperator.split("---");
				var seperator = (filename_seperator[1]) ? filename_seperator[1] : ',';
				require('./utils').readFileAsText(filename_seperator[0], (csv_string) => {
					var data_array = csv_string.split(/\r\n|\n/);
					var data_object = {};
					var colnames;
					data_array.forEach((line, row_index) => {
						//catch lines at end without data
						if ((line=="") || (RegExp("^"+seperator+"*$").test(line.trim()))) {
							console.log("Line "+row_index+" has no data.");
							return;
						}
						//normal data lines
						else {
							var tmp_line_array = line.split(seperator);
							if (row_index==0) { //first line should contain column names
								colnames = tmp_line_array;
								tmp_line_array.forEach((item, col_index) => {
									data_object[item] = [];
								});
								//console.log(colnames);
							}
							//use global.interesting_cases to filter
							/*else if ((!global.interesting_cases[tmp_line_array[0]])
							|| (!global.interesting_cases[tmp_line_array[0]][tmp_line_array[1].split(' ')[0]])) {}*/
							//else if (row_index>10000){} //possibility to limit number of rows for testing stuff with smaller dataset
							else {
								tmp_line_array.forEach((item, col_index) => {
									if (item=="NULL") {
										item=null;
									}
									data_object[colnames[col_index]].push(item);
								});
							}
						}
					});
					completed_callback(con_id, true);
					var all_cols_have_same_length = true;
					colnames.forEach((colname) => {
						if (data_object[colname].length!=data_object[colnames[0]].length) {
							all_cols_have_same_length = false;
						}
					});
					if (all_cols_have_same_length) {
						data_object[require('./utils').placeholders.const_dataset_row] = data_object[colnames[0]].map(() => 1);//add column to be used as dataset_row in MMs
						data_callback(con_id, auth, data_object, false, true);
					}
					else {
						console.warn("The CSV seems corrupt. Not all data columns have the same length.");
					}
					console.log("Reading done ("+filename_seperator[0]+")");
				}, true);
			}
			//end actual adapter interface

			return self;
		}

		return self;
	}

})(typeof exports === 'undefined'? this['openEHR']={}: exports);
