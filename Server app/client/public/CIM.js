/*
	class representing a clinical information model - so basically everything necessary to derive MMs from a CIM and to provide information about a CIM for that purpose to the user
*/
function CIM(origin, xml, id = false, createdOn = false) {
	var self = this;
	self.origin; //string - where does the template come from? (repository, drag and dropped into browser)
	self.xml; //xmlObject - e.g. opt-template or archetype as xml export
	self.isArchetype; //bool - specifies if this is an archetype or template
	self.id; //string - template_id or archetype_id
	self.createdOn;//string - when this CIM was created according to its xml
	self.atCodes; //object - atCodes defined in this CIM
	self.MMs; //ko.observableArray - measurement methods derived from this CIM
	//hint: mind the constructor at the end of the class definition

	/*
		methods
	*/

	self.try_push_to_MMs = function(iMM) {
		if (!self.MMs().find(function(MMFromList) {
			return (iMM.equals_raw(MMFromList));
		})) {
			self.MMs.push(iMM);
		}
		else {
			/*self.message_count = (self.message_count) ? self.message_count+1 : 1;
			if (self.message_count<50) {
				//console.log("Duplicate MM from CIM:");
				//console.log(iMM);
				var pathExtension = (iMM.domainPaths[0].pathExtension) ? iMM.domainPaths[0].pathExtension.asTextToAppend : "";
				console.log(iMM.domainPaths[0].domainPath + pathExtension + '('+iMM.domainPaths.length+')');
			}*/
		}
	}

	/*
		 Parsing relies on following openEHR specification versions:
		 ADL 1.4
		 AOM 1.4
		 RM 1.04
		 BASE 1.1.0 (also changes here may not do much harm)
	*/
	self.processCIM = async function() {
		var definition = $(self.xml).find("definition")[0];
		self.isArchetype = ($(self.xml).find("template").length==0) ? true : false;
		//atCode definitions per archetype
		self.extractTermDefinitions();
		//retrieve value set, range, format and cardinality constraints from template
		self.extractAOMCardinalityConstraints(definition);
		self.extractAOMPrimitiveTypeConstraints(definition);
		self.extractAOMDomainTypeConstraints(definition);
		self.extractRMDataTypeConstraints(definition);
		return self;
	}

	/* extracts atCode definitions from <term_definitions>-Elements and adds them to given archteypes object
	{archetypeId: {atCodes: {atXXXX: Aaaaaaa, atYYYY: Bbbbbbb}}}
	*/
	self.extractTermDefinitions = function() {
		//if CIM is archetype
		if (self.isArchetype) {
			var archetypeId = self.id.toLowerCase().trim();
			var termDefs = $(self.xml).find("term_definitions[language$='en']")[0];
			$(termDefs).find("items[code]")
				.each(function(index, elem) {
					self.atCodes[archetypeId] = (self.atCodes[archetypeId]) ? self.atCodes[archetypeId] : {};
					self.atCodes[archetypeId]["atCodes"] = (self.atCodes[archetypeId]["atCodes"]) ? self.atCodes[archetypeId]["atCodes"] : {};
					self.atCodes[archetypeId]["atCodes"][elem.attributes['code'].value] = $(elem).children("#text")[0].textContent;
			});
		}
		//if its an opt-template
		else {
			var def = $(self.xml).find("definition")[0];
			$(def).find("term_definitions")
			.each(function(index, elem) {
				var archetypeId = $(elem).parent().children("archetype_id")[0].textContent.toLowerCase().trim();
				self.atCodes[archetypeId] = (self.atCodes[archetypeId]) ? self.atCodes[archetypeId] : {};
				self.atCodes[archetypeId]["atCodes"] = (self.atCodes[archetypeId]["atCodes"]) ? self.atCodes[archetypeId]["atCodes"] : {};
				self.atCodes[archetypeId]["atCodes"][elem.attributes['code'].value] = $(elem).children("#text")[0].textContent;
			});
		}
	}

	/* 	reads information from an openEHR Interval into a check for a MM
		@return: string condition for MM
	*/
	self.intervalToCondition = function (iIntervalStructure) {
		var check = "";
		var and = "";
		if  ($(iIntervalStructure).children("lower_unbounded")[0].textContent == 'false') {
			var operator = ($(iIntervalStructure).children("lower_included")[0].textContent == 'true') ? ">=" : ">";
			check = "item0 " + operator + " " + $(iIntervalStructure).children("lower")[0].textContent;
			and = " && ";
		}
		var upper = "";
		if  ($(iIntervalStructure).children("upper_unbounded")[0].textContent == 'false') {
			var operator = ($(iIntervalStructure).children("upper_included")[0].textContent == 'true') ? "<=" : "<";
			check +=  and + "item0 " + operator + " " + $(iIntervalStructure).children("upper")[0].textContent;
		}
		return check;
	}

	/* 	reads information from a simple openEHR element-list into a check for a MM (i.e. a check if a value in dataset is in list of values specified by elements in template) - lists in openEHR can also be more complex, cf. C_DV_QUANTITY, which specifies lists of 	type C_QUANTITY_ITEM which has more attributes than just one value
		@return: string condition for MM
	*/
	self.listToCondition = function (listContainingNode, elementsAreStrings = false, list_node_selector = "list", check = "is.element(item0, c(<replace>))") {
		//attention representation in opts seems strange: e.g. C_INTEGER says list attribute is 0..1, XML-file looks like(<list>0</list>, <list>1</list>,<list>2</list>,<list>4</list>) wtf? But at least it seems to be always like that
		//example of final check in R: match(item0, c(0,1,2,4))
		var tmpArray = [];
		$(listContainingNode).find(list_node_selector).each(function(index2, elem2) {
			tmpArray.push(elem2.textContent);
		});
		//if item has list-items then add constraint
		if (tmpArray.length>0) {
			var tick = (elementsAreStrings) ? '"' : '';
			return check.replace('<replace>', tick + tmpArray.join(tick + ',' + tick) + tick);
		}
		else
			return false;
	}

	/* gets archetype-path for given node in xml-structure */
	self.getArchetypePath =  function(iNode) {
		var path = addNodeToPath(iNode);
		$(iNode).parentsUntil('template, archetype').each(function(index, elem) {
			path = addNodeToPath(elem, path);
		});
		//catch [at0000] path begin if CIM is just an archetype xml-export
		if (self.isArchetype) {
			path = path.replace(/^\[at0000\]/, "["+self.id+"]");
		}
		return path;

		function addNodeToPath(iiNode, iPath = false) {
			if (['children', 'definition'].includes(iiNode.nodeName)) {
				var nameConstraint = "";
				var tmp = $(iiNode).children("archetype_id");
				if (tmp.length == 0) {
					tmp = $(iiNode).children("node_id");
					//check if node has a fixed name, e.g. path should not be ..../items[at0001]/... instead: ..../items[at0001, fixedName]/...
					var nameConstraintNode = $(iiNode).children("attributes").children("rm_attribute_name:contains('name')").parent().children("children").children("attributes").children("children").children("rm_type_name:contains('STRING')").parent().children("item[xsi\\:type='C_STRING']");
					//if there is a constraintNode constraining the name
					if (nameConstraintNode.length == 1) {
						var possibleNames = self.listToCondition(nameConstraintNode, true).replace('is.element(item0, c("', '').replace('"))', '');
						//if there is just one permissible name
						if (possibleNames.indexOf(",")==-1) {
							nameConstraint = ",'"+possibleNames+"'";
						}
					}
				}
				if (tmp[0].textContent.length > 0) {
					var tmpStr = '['+ tmp[0].textContent.trim() + nameConstraint + ']';
					iPath = (iPath) ? tmpStr + iPath : tmpStr;
				}
			}
			else if (iiNode.nodeName == 'attributes') {
				var tmpStr = $(iiNode).children("rm_attribute_name")[0].textContent.trim();
				iPath = (iPath) ? "/" + tmpStr + iPath : "/" + tmpStr;
			}
			else if (iiNode.nodeName == 'template') {
				iPath = "";
			}
			return iPath;
		}
	}

	/* The checks for the different openEHR-AOM-constraints expressing some kind of cardinality explained:
	existence:
		-checks if count of parentObj.Attribute is in range specified in existence. In the example: if there are between 0 and 1 attributes with attributeName
	cardinality:
		- checks if the count of Attribute.childNodes is in range of cardinality. In example: if the container-attribute has more than 0 childNodes of any type
	occurrences
		-checks if the count of ParentAttribute.childNodes of node type specified by predicate  is in occurrence-range. In example:  exactly one node of this type
	occurrences for nodes with no siblings (empty node id <node_id/>)
		- checks if the count of ParentAttribute.childNode is in specified occurrences range. In example: if the parent-attribute has exacatly one node-object as child
		- More about the empty node_id stuff: "any leaf or near-leaf node which has no sibling nodes from the same attribute can safely have no node_id" [https://specifications.openehr.org/releases/AM/latest/AOM1.4.html#_archetype_constraint_class] - this is why sometimes there are structures with empty <node_id/>. Example: the common attribute "name" (inherited from locatable) is of type DV_TEXT. Therefore, in the archetype definition the C_Attibute "name" has a childnode of <rm_type_name>DV_TEXT (or its subtype DV_CODED_TEXT). These nodes inherit the "occurences" attribute from C_OBJECT.
	*/
	/* extracts constraints regarding cardinality, i.e.: occurences, existence and cardinality interval values as check for MM*/
	self.extractAOMCardinalityConstraints = function(iSubtree) {
		$(iSubtree).find("existence, cardinality, occurrences")
		.each(function(index, elem) {
			//parse range check from interval
			//get interval node for cardinality - for existence and occurences elem already is the node
			var intervalStructure = elem;
			if (elem.nodeName == 'cardinality') {
				intervalStructure = $(elem).children("interval")[0];
			}
			var check = self.intervalToCondition(intervalStructure);

			//set instruction for value-vector construction
			var pathExtension = null;
			var getParent = false;
			if (elem.nodeName == 'existence') {
				pathExtension = {countAttribute: $(elem).parent().children("rm_attribute_name")[0].textContent, asTextToAppend: ".countAttribute("+$(elem).parent().children("rm_attribute_name")[0].textContent+")"};
				getParent = true;
			}
			else if  (elem.nodeName == 'cardinality') {
				pathExtension = {countChildren: "any_child", asTextToAppend: ".countChildnodes(any_child)"};
			}
			else if (elem.nodeName == 'occurrences') {
				var node_id = $(elem).parent().children("node_id");
				if ($(elem).parent().children("archetype_id").length>0) node_id = $(elem).parent().children("archetype_id"); //if it is an archetype-root at0000 won't be the archetype_node_id in the dataset. It will be the archetype_id, so this needs to be the predicate
				if ((node_id) && (node_id[0].textContent != "")) { //normal occurrences
					pathExtension = {countChildren: node_id[0].textContent, asTextToAppend: ".countChildnodes("+ node_id[0].textContent +")"};
					getParent = true;
				}
				else { //occurrences with empty node_id
					pathExtension = {countChildren: "only_child", asTextToAppend: ".countChildnodes(only_child)"};
				}
			}
			//retrieve correct domainPath
			var tmpDomainPath =  self.getArchetypePath(elem);
			if (getParent) {
				tmpDomainPath = openEHR.get_parent_path(tmpDomainPath);
			}
			//add MM
			self.try_push_to_MMs(new MM("check,cardinality", [{domainPath: tmpDomainPath, pathExtension: pathExtension, datatype:"numeric", item:"item0"}], "mean", check));
		});
	}

	/* extracts constraints on AOM primitive types, which mostly are constraints regarding the values (of leaf nodes), i.e.: how does a valid value in a path look like (value sets, range, matching a pattern etc.) */
	self.extractAOMPrimitiveTypeConstraints = function(iSubtree) {
		//C_PRIMITIVE Class - is abstract (so, there is nothing to parse ;-)

		//C_BOOLEAN have mandatory attributes .true_valid .false_valid
		$(iSubtree).find("item[xsi\\:type='C_BOOLEAN']")
		.each(function(index, elem) {
			var tmpArray = [];
			if ($(elem).children("true_valid")[0].textContent == 'true') tmpArray.push("true");
			if ($(elem).children("false_valid")[0].textContent == 'true') tmpArray.push("false");
			//just create set if there is a restriction on valid values (no sense if true and false both are fine)
			if (tmpArray.length==1) self.try_push_to_MMs(new MM("check,allowed_bool", [{domainPath: self.getArchetypePath(elem), datatype:"logical", item:"item0"}], "mean", "item0 == " + tmpArray[0].toUpperCase()));
		});

			//C_STRING optional .pattern (regex), optional .list, mandatory .list_open (but opts don't care, never seen this field, treat as optional)
		$(iSubtree).find("item[xsi\\:type='C_STRING']")
		.each(function(index, elem) {
			//pattern
			/* check-function example:
				check = function(item) {
				  item = item[domainPaths[1]]
				  grepl("<replace>", c(item0), perl=TRUE)
				}
			*/
			var tmpPattern = $(elem).children("pattern");
			if (tmpPattern.length>0) {
				var check = 'grepl("<replace>", c(item0), perl=TRUE)';
				tmpPattern = tmpPattern[0].textContent.replace(/\\/g, "\\\\");  //escape single backslash to \\ for R regex
				self.try_push_to_MMs(new MM("check,pattern", [{ domainPath: self.getArchetypePath(elem), datatype:"string", item:"item0"}], "mean", check.replace('<replace>', tmpPattern)));
			}
			//list and list_open
			var tmpListOpen = $(elem).children("list_open");
			if ((tmpListOpen.length == 0) || ((tmpListOpen.length>0) && (tmpListOpen[0].textContent == "false")))  {
				var tmpCheck = self.listToCondition(elem, true);
				if (tmpCheck) self.try_push_to_MMs(new MM("check,string_list", [{domainPath: self.getArchetypePath(elem), datatype:"string", item:"item0"}], "mean", tmpCheck));
			}
		});

		//C_INTEGER and C_REAL have optional attributes .range and .list
		$(iSubtree).find("item[xsi\\:type='C_INTEGER'], item[xsi\\:type='C_REAL']")
		.each(function(index, elem) {
			//process range constraints
			$(elem).find("range").each(function(index2, elem2) {
				self.try_push_to_MMs(new MM("check,range", [{domainPath: self.getArchetypePath(elem), datatype:"numeric", item:"item0"}], "mean", self.intervalToCondition(elem2)));
			});
			//process list constraints
			var tmpCheck = self.listToCondition(elem);
			if (tmpCheck) self.try_push_to_MMs(new MM("check,numeric_list", [{ domainPath: self.getArchetypePath(elem),  datatype:"numeric", item:"item0"}], "mean", tmpCheck));
		});

		//C_DATE, C_TIME and C_DATE_TIME- at least ocean archetype designer serializations differ from specifications - simply uses pattern for "validity"-stuff
		//representation differs from specs and ocean's archetype designer only supports the pattern stuff, not the range constraint from AOM, which i deem the most useful constraint. JavaScript has good support for ISO8601, so i just assume that range constraint will be ISO8601 values for upper and lower and directly parsable by JavaScript Date class. This range check is all i will implement for now.
		$(iSubtree).find("item[xsi\\:type='C_DATE_TIME']")
		.each(function(index, elem) {
			$(elem).find("range").each(function(index2, elem2) {
				//create range check were upper/lower values are UNIX/POSIX time values, i.e. seconds since 1.1.1970 00:00:00
				var tmpCheck = self.intervalToCondition(elem2).replace(/ (\d|-|T|:|\.|Z)+/g, function(match) {
					return ' ' + Math.round((new Date(match.trim())).getTime() / 1000);
				});
				self.try_push_to_MMs(new MM("check,date_time_range", [{ domainPath: self.getArchetypePath(elem), pathExtension: {asUnixTS: true, asTextToAppend: ".asUnixTS"}, datatype:"numeric", item:"item0"}], "mean", tmpCheck));
			});
		});

		//C_DURATION - at least ocean archetype designer serializations differ from specifications - simply uses pattern for "allowed"-stuff (.pattern .range)
		//(since this is the tool i currently create my archetypes with, this is what i parse here)
		//since this whole solution to represent time and duration things is awkward to me, consider everything on that as "doubtful" until reasonably tested
		$(iSubtree).find("item[xsi\\:type='C_DURATION']")
		.each(function(index, elem) {
			//extract pattern, example: grepl("P(?=(\\d|[YMWDTHMS\\.])+)(T?(?=(\\d|[YMWDTHMS\\.])+)(\\d+(.\\d+)?S)?)?$", c(item0), perl=TRUE)
			var tmpPattern = $(elem).children("pattern");
			if (tmpPattern.length>0) {
				var check = 'grepl("<replace>$", c(item0), perl=TRUE)';
				tmpPattern = tmpPattern[0].textContent;
				if (tmpPattern.indexOf("T")>-1) tmpPattern += ")?";
				tmpPattern = tmpPattern
					.replace("Y", "(\\d+(.\\d+)?Y)?") //if there is a Y in pattern then a value, e.g. 9.3, for years can occur but doesn't have to
					.replace(/M/g, "(\\d+(.\\d+)?M)?") //captures month and minutes!
					.replace("W", "(\\d+(.\\d+)?W)?")
					.replace("D", "(\\d+(.\\d+)?D)?")
					.replace("H", "(\\d+(.\\d+)?H)?")
					.replace("S", "(\\d+(.\\d+)?S)?")
					.replace("T", "(T?(?=(\\d|[YMWDTHMS\\.])+)") // ensure that there is at least some value behind (closing bracket is added in begin)
					.replace("P", "P(?=(\\d|[YMWDTHMS\\.])+)") // ensure that there is at least some value behind (not just a P)
					.replace(/\\/g, "\\\\");
				self.try_push_to_MMs(new MM("check,duration_pattern", [{domainPath: self.getArchetypePath(elem),  datatype:"string", item:"item0"}], "mean", check.replace('<replace>', tmpPattern)));
			}
			//process range (optional attribute) - simply assume just one "level" (e.g. duration in days, not in weeks and days), since some combinations simply can't be compared. (Furthermore, ocean archetype designer only supports just one number in range constraint) - so, the modeller of the archetype has to ensure sensible constraints anyway
			$(elem).find("range").each(function(index2, elem2) {
				var tmpCheck = self.intervalToCondition(elem2);
				if (tmpCheck == "") return; //catch interval with no actual contraint, i.e. lower and upper unbound
				var matches = tmpCheck.match(/PT?\d+(\.\d+)?[YMWDHS]/g);
				var designator = matches[0][matches[0].length-1];
				var comparableUnit = "seconds";
				if (designator == "Y") {
					comparableUnit = "years";
				}
				else if (designator == "M") {
					if (/T/.test(tmpCheck)) {//check if T is in pattern to distinguis month from minutes
						comparableUnit = "minutes";
					}
					else {
						comparableUnit = "month";
					}
				}
				else if (designator == "D") {
					comparableUnit = "days";
				}
				else if (designator == "H") {
					comparableUnit = "hours";
				}

				var tmpCheck = tmpCheck.replace(/P/g, '').replace(/T/g, '').replace(new RegExp(designator, "g"), '');
				self.try_push_to_MMs(new MM("check,duration_range", [{ domainPath: self.getArchetypePath(elem),  pathExtension: {durationIn: comparableUnit, asTextToAppend: ".durationIn("+ comparableUnit +")"}, datatype:"numeric", item: "item0"}], "mean", tmpCheck.replace(/\\/g, "\\\\")));
			});
		});
	}

	/* extracts constraints derived from AOM classes inherited from C_DOMAIN_TYPE  - i.e. domain-specific extensions */
	self.extractAOMDomainTypeConstraints = function(iSubtree) {
		//unfortunately (as at 05/2019) AOM 1.4 documentation does not go into detail on defined domain-specific extension classes and JSON-AOM1.4 specifications are empty. Solely XSD-AOM specs exist.

		//C_CODE_PHRASE - .terminology_id (TERMINOLOGY_ID .value (string)) 0..1, .code_list (xs:string) 0..*
		$(iSubtree).find("children[xsi\\:type='C_CODE_PHRASE']").each(function(index, elem) {
			var terminologyId = false;
			//is specified terminology_id of element in template the same as in dataset?
			$(elem).find("terminology_id").each(function(index2, elem2) {
				terminologyId = $(elem2).children("value")[0].textContent;
				self.try_push_to_MMs(new MM("check,coded_phrase", [{ domainPath: self.getArchetypePath(elem)+"/terminology_id/value", datatype:"string", item: "item0"}], "mean", 'item0 == "' + terminologyId + '"'));
			});
			//is code_string of element in dataset in code_list in template?
			var tmpCheck = self.listToCondition(elem, true, "code_list");
			if (tmpCheck) self.try_push_to_MMs(new MM("check,coded_phrase", [{ domainPath: self.getArchetypePath(elem)+"/code_string", item: "item0", datatype:"string"}], "mean", tmpCheck));
			//does value of element in dataset correspond to a resolved string of code_list in template? - since value sets with terminology_id "local" are not too unlikely to change
			if ((tmpCheck) && (terminologyId == "local")) {
				//resolve atCodes in code_list
				var archetypeId = false;
				//get archetypeId - for each string in squared brackets "[xxxx]" in path check if its an archetype-Id and set first
				self.getArchetypePath(elem).match(/(\[.*?\])/g).forEach(function(elem) {
					if (!elem.match(/\[at.+?\]/)) { // store archetype-Id - we just want the last one, so its fine to overwrite
						archetypeId = elem.replace('[', '').replace(']', '').toLowerCase();
					}
				});
				//replace all atCodes in tmpCheck with
				tmpCheck = tmpCheck.replace(/at(\d|\.)+/g, function(atCode) {
					return self.atCodes[archetypeId]["atCodes"][atCode];
				});
				//pathExtension: ".parent/value"
				self.try_push_to_MMs(new MM("check,coded_phrase", [{ domainPath: openEHR.get_parent_path(self.getArchetypePath(elem)) + "/value", item: "item0", datatype:"string"}], "mean", tmpCheck));
			}
		});

		//C_DV_ORDINAL
		$(iSubtree).find("children[xsi\\:type='C_DV_ORDINAL']").each(function(index, elem) {
			//check if integer-value of ordinal is in defined value set
			var tmpCheck = self.listToCondition(elem, false, "list > value");
			if (tmpCheck) {
				var item1DomainPath = self.getArchetypePath(elem);
				self.try_push_to_MMs(new MM("check,ordinal_value", [{ domainPath: self.getArchetypePath(elem) + "/value", pathExtension: null,datatype:"numeric", item:"item0"}], "mean", tmpCheck)); //e.g. index = match(item1, c(0,1,2))
				//check if the symbol values (value, terminology_id, code_string) are the right ones for the ordinal's integer-value
				var indexPart = 'index = match(item1, ' + tmpCheck.match(/c\((\d|,)*\)/)[0] + ')\n';
				//symbol/value, i.e. the textual value of the ordinal, e.g. "Yes"
				var extendedCheck = indexPart + 'symbol_values = c(<replace>)\n' + 'symbol_values[index] == item0';
				tmpCheck = self.listToCondition(elem, true, "symbol > value", extendedCheck);
				if (tmpCheck) self.try_push_to_MMs(new MM("check,ordinal_symbol", [{ domainPath: self.getArchetypePath(elem) + "/symbol/value", pathExtension: null,datatype:"string", item:"item0"}, { domainPath: item1DomainPath + "/value", item: "item1", datatype:"numeric"}], "mean", tmpCheck));
				//terminology used to define the ordinal
				var extendedCheck = indexPart + 'terminology_ids = c(<replace>)\n' + 'terminology_ids[index] == item0';
				tmpCheck = self.listToCondition(elem, true, "terminology_id > value", extendedCheck);
				if (tmpCheck) self.try_push_to_MMs(new MM("check,ordinal_symbol", [{ domainPath: self.getArchetypePath(elem) + "/symbol/defining_code/terminology_id/value", item: "item0", datatype:"string"}, { domainPath: item1DomainPath + "/value", item: "item1", datatype:"numeric"}], "mean", tmpCheck));
				//at-code (maybe in multilingual setting this is correct even if symobol/value isn't)
				var extendedCheck = indexPart + 'code_strings = c(<replace>)\n' + 'code_strings[index] == item0';
				tmpCheck = self.listToCondition(elem, true, "defining_code > code_string", extendedCheck);
				if (tmpCheck) self.try_push_to_MMs(new MM("check,ordinal_symbol", [{ domainPath: self.getArchetypePath(elem) + "/symbol/defining_code/code_string", item: "item0", datatype:"string"}, { domainPath: item1DomainPath + "/value", pathExtension:null, item: "item1", datatype:"numeric"}], "mean", tmpCheck));
			}
		});

		//C_DV_QUANTITY (property defines the type of scientific quantity. cf. https://github.com/openEHR/terminology/blob/master/openEHR_RM/en/openehr_terminology.xml) - no ideas for obvious checks (property doesn't seem to be expressed in dataset)
		//derive checks from list
		$(iSubtree).find("children[xsi\\:type='C_DV_QUANTITY']").each(function(index, elem) {
			//check if units-value of QUANTITY is in defined value set
			//check if magnitude lies in optional range for given unit and
			//check if precision value lies in optional range for given unit
			var tmpArray = [];
			var tmpArrayM = [];
			var tmpArrayP = [];
			$(elem).find("list").each(function(index2, elem2) {
				var currentUnit = false;
				$(elem2).find("units").each(function(index3, elem3) {
					currentUnit = elem3.textContent;
					tmpArray.push(currentUnit);
				});
				$(elem2).find("magnitude").each(function(index3, elem3) {
					tmpArrayM.push('if (item1 == "' + currentUnit + '") {return (' + self.intervalToCondition(elem3) + ')}' );
				});
				$(elem2).find("precision").each(function(index3, elem3) {
					tmpArrayP.push('if (item1 == "' + currentUnit + '") {return (' + self.intervalToCondition(elem3) + ')}' );
				});
			});
			//if arrays have items then add respective constraints
			if (tmpArray.length>0) {
				//units
				var item1DomainPath = self.getArchetypePath(elem);
				self.try_push_to_MMs(new MM("check,quantity_units", [{ domainPath: item1DomainPath + "/units", item: "item0", datatype:"string"}], "mean", "is.element(item0, c(<replace>))".replace('<replace>', '"' + tmpArray.join('","') + '"')));
				//magnitude range
				if (tmpArrayM.length>0) {
					self.try_push_to_MMs(new MM("check,quantity_magnitude_ranges", [{ domainPath: self.getArchetypePath(elem) + "/magnitude", item: "item0", datatype:"numeric"}, { domainPath: item1DomainPath + "/units", item: "item1", datatype:"string"}], "mean", tmpArrayM.join('\n')));
				}
				//precision range
				if (tmpArrayP.length>0) {
					self.try_push_to_MMs(new MM("check,quantity_precision_ranges", [{ domainPath: self.getArchetypePath(elem) + "/precision", item: "item0", datatype:"numeric"}, { domainPath: item1DomainPath + "/units", item: "item1", datatype:"string"}], "mean", tmpArrayM.join('\n')));
				}
			}
		});

		//C_DV_STATE - postponed since no example template or data available and oceans modelling tools seem to not even support it
		/*
		C_DV_STATE
		STATE_MACHINE
		STATE
		NON_TERMINAL_STATE
		TERMINAL_STATE
		TRANSITION
		*/
	}

	/* extracts constraints derived from RM data_types in combination with template at hand*/
	self.extractRMDataTypeConstraints = function(iSubtree) {
		//check if rm_type_name and value in dataset /@class-pathes fit together -  attention, how and of rm_type_name is delivered with dataset doesn't seem to be defined in openEHR, so its probably a platform (Think/Better) specific thing
		$(iSubtree).find("rm_type_name")
		.each(function(index, elem) {
			self.try_push_to_MMs(new MM("check,rm_type_name", [{ domainPath: self.getArchetypePath(elem) + "/@class", item: "item0", datatype:"string"}], "mean", 'item0 == "' + elem.textContent+'"'));
		});

		/* (low priority) there are more constraints derivable from DV-Types in RM:
		CODE_PHRASE
		DATA_VALUE
		DV_ABSOLUTE_QUANTITY
		DV_AMOUNT
		DV_BOOLEAN
		DV_CODED_TEXT
		DV_COUNT
		DV_DATE
		DV_DATE_TIME
		DV_DURATION
		DV_EHR_URI
		DV_ENCAPSULATED
		DV_GENERAL_TIME_SPECIFICATION
		DV_IDENTIFIER
		DV_INTERVAL
		DV_MULTIMEDIA
		DV_ORDERED
		DV_ORDINAL
		DV_PARAGRAPH
		DV_PARSABLE
		DV_PERIODIC_TIME_SPECIFICATION
		DV_PROPORTION - .type
		DV_QUANTIFIED
		DV_QUANTITY
		DV_STATE - .is_terminal - hard and not too important - skip for now
		DV_TEMPORAL
		DV_TEXT
		DV_TIME
		DV_TIME_SPECIFICATION
		DV_URI
		PROPORTION_KIND
		REFERENCE_RANGE
		TERM_MAPPING
		*/
	}

	/*
		constructor
	*/

	//if is created from xml-string or just from one row in template api result
	//self.CIMs.push(new CIM("platform", null, iResults.templates[i].templateId, iResults.templates[i].createdOn));
	//var aCIM = new CIM("file", iString, true)
	if (typeof origin == 'string') {
		self.origin = origin;
		self.xml = (xml) ? $.parseXML(xml) : xml;
		self.isArchetype = null;

		//if applicable parse infos from xml
		if (((!id) || (!createdOn)) && (xml)) {
			//parse some general information from template
			id = $(self.xml).find("template_id, archetype_id")[0].textContent.trim(); //templateId
			try {
				createdOn = $(self.xml).find("original_author[id='date']")[0].textContent; //creation date
			}
			catch {
				createdOn = "Not specified";
			}
		}

		self.id = id; //template_id or archetype_id
		self.createdOn = createdOn;
		self.atCodes = {}; //atCodes defined in this CIM
		self.MMs = ko.observableArray(); //measurement methods derived from this CIM

		if (self.xml) {
			self.processCIM();
		}
	}
	// load from object structure created using getObjectValues function
	else {
		self.origin = origin.origin;
		self.xml = $.parseXML(origin.xml);
		self.isArchetype = origin.isArchetype;
		self.id = origin.id;
		self.createdOn = origin.createdOn;
		self.atCodes = origin.atCodes;
		self.MMs = ko.observableArray();
		origin.MMs.forEach(function(iMM) {
			self.MMs.push(new MM(iMM));
		});
	}
}
