const express = require('express');
const bodyParser = require('body-parser');
const child_process = require('child_process');

const app = express();

//TODO 6 - proper session handling solution
global.contexts = {};
global.connections = {};

//for client delivery
const clientAppPath = __dirname + '/client';
app.use(express.static(clientAppPath + '/public'));
app.use(bodyParser.json({limit: '50mb'}));

//required stuff
const config = require('./client/public/config');
const utils = require('./client/public/utils');
const openEHR = require('./client/public/openEHR');
const dev = require('./client/public/dev');

//delete all plots from previous sessions
utils.clearDir(clientAppPath + '/public/plots/', [".gitkeep"]);

//set R-Template
var RTemplate;
utils.readFileAsText("/MM_templates/MM_template.r", function(iTemplate) {
	RTemplate = iTemplate;
});

/* NOTE 0 - whenever touching sth. add proper testing and clean code
	- restructure where necessary and rename ...
	- keep it simple and plain, i.e. short simple functions not in big nested structures etc.
		- hardly ever longer than 20 lines or "explainable in one sentence"
		- usually not more than two nestings or "cognitive complexity factor" > 3
		- explicitly pass and return values instead of having nested structures where I access var in parent scope etc.
	- (get rid of camel case, i.e. my_shiny_function instead of myShinyFunction)
	- use async and await instead of callbacks
	- make use of design patterns
*/

//TODO 8 data cleansing and data export functionalities could be a useful thing too (e.g. to enable reproducible data cleansing with KB)

/*
	handling routes (i use a kind of "verbose" construct simply to have named functions. I can handle them better with dev.js and testing. Maybe there is a more elegant way i don't know of...)
*/

/*	returns client-app - client called GET / 	*/
app.sendClient = (function(route = '/') {
	var returnFunction = function (req, res) {
		if (dev.is) dev.initServerApp(app, dev, req, utils);
		res.sendFile(clientAppPath + '/client.html');
	}
	app.get(route, returnFunction);
	return returnFunction;
})();

//TODO 9 Clinical knowledge mangager also has a REST_API - retrieving CIMs from CKM might be even more useful - however it does not seem like anybody uses CIMs to govern MMs
/*	retrieves returns available templates - client called GET /templates 	*/
app.getTemplates = (function(route = '/templates') {
	var returnFunction = async function (req, res) {
		var templates = await (new openEHR.REST_API_ADAPTER(config.openEHRServer)).get_templates(req.headers.authorization);
		app.respond_to_request(res, templates);
	}
	app.get(route, returnFunction);
	return returnFunction;
})();

/*	retrieves and returns template opt - client called GET /template/<template_id> 	*/
app.getTemplate = (function(route = '/template/*') {
	var returnFunction = async function (req, res) {
		var template = await (new openEHR.REST_API_ADAPTER(config.openEHRServer)).get_template_opt(
			req.headers.authorization,
			req.originalUrl.replace("/template/","")
		);
		res.statusCode = 200;
		res.setHeader('Content-Type', 'application/xml'); //careful its xml!
		res.end(template);
	}
	app.get(route, returnFunction);
	return returnFunction;
})();

/*	returns knowledge base for tags - client called GET /kb 	*/
app.getKnowledgebase = (function(route = '/kb') {
	var returnFunction = function (req, res) {
		var wisdom = [];
		var files_read = 0;
		const fs = require('fs');
		const dir = './client/public/knowledge_base/';
		// loop through all files in the directory
		fs.readdir(dir, (err, files) => {
	    if (err) {
			  throw err;
	    }
	    files.forEach(file => {
				utils.readFileAsText("/knowledge_base/" + file, function(iWisdom) {
					//only if there is knowledge to add
					if (iWisdom!="") {
						wisdom.push(JSON.stringify(utils.parse_knowledge_base_file(file, iWisdom)));
					}
					files_read++;
					//if its the last file send response
					if (files_read==files.length) {
						res.statusCode = 200;
						res.setHeader('Content-Type', 'application/json');
						wisdom = '[' + wisdom.join(',') + ']';
						res.end(wisdom);
					}
				});
	    });
		});
	}
	app.get(route, returnFunction);
	return returnFunction;
})();

/*	requests data from openEHR-data repository, derives data-paths and returns them - client called POST /paths */
app.process_query = (function(route = '/query') {
	var returnFunction = function (req, res) {
		try {
			var adapter = new openEHR.REST_API_ADAPTER(config.openEHRServer);
			var aql = (adapter.filebased) ? req.body.aql : openEHR.AQL(req.body.aql); //parse query to enable its manipulation
			app.create_empty_context(req.headers.authorization, req.body.aql);
			app.respond_to_request(res, {});//send positive response, primarily indicating that parsing AQL did not throw errors
		}
		catch (error) {
			console.error(error);
			app.respond_to_request(res, "(Detailed message on server) " + error.toString(), 400);//send error response
		}
		var completed_callback = function(con_id, content) {
			app.send_SSE_to_client(con_id, JSON.stringify({retrieving_data_completed:content}));
		}
		if (adapter.filebased) {
			adapter.get_data(req.body.connection_id, req.headers.authorization, aql, app.data_callback, completed_callback);
		}
		else {
			adapter.get_data_in_pages(
			req.body.connection_id,
			req.headers.authorization,
			aql,
			Math.max(0, aql.offset),
			req.body.max_pagesize,
			aql.fetch,
			app.data_callback,
			completed_callback
		);
		}
	}
	app.post(route, returnFunction);
	return returnFunction;
})();

/*	returns the first result row in context of requesting user that matches given condition or first row without condition */
app.get_result_row = (function(route = '/result_row') {
	var returnFunction = function (req, res) {
		try {
			var res_val = global.contexts[req.headers.authorization].data.find((element, index) => {
				return (parseInt(req.body.condition) == index) ? true : (JSON.stringify(element).indexOf(req.body.condition)>-1);
			});
			if (!res_val) {
				res_val = "No matching row";
			}
			app.respond_to_request(res, res_val);
		}
		catch (error) {
			console.error(error);
			app.respond_to_request(res, "(Detailed message on server) " + error.toString(), 400);//send error response
		}
	}
	app.post(route, returnFunction);
	return returnFunction;
})();

/*	opens the connection for server-sent events to push results back to client - client called POST /results */
app.results = (function(route = '/results') {
	var returnFunction = function (req, res) {
		res.writeHead(200, {
			'Content-Type': 'text/event-stream',
			'Cache-Control': 'no-cache',
			'Connection': 'keep-alive'
		});
		var tmp_id = utils.generateId(8);
		res.start_time = new Date();//probably this is redundant, but I failed to figure out where I can get it from
		global.connections[tmp_id] = res;
		app.send_SSE_to_client(tmp_id, tmp_id);
		setTimeout(function(){app.keep_alive(tmp_id)},10000);
	}
	app.get(route, returnFunction);
	return returnFunction;
})();

/*	executes given MM from request - client called POST /executeMM */
app.executeMM = (function(route = '/executeMM') {
	global.startet_first_MM = (global.startet_first_MM) ? global.startet_first_MM : Date.now();
	var returnFunction = async function (req, res) {
		var that = {
			hash: req.body.mm_hash,
			context: global.contexts[req.headers.authorization],
			con_id: req.body.connection_id,
			domain_paths: req.body.mmParameters.domainPaths
		};
		//check if request has valid credentials
		if (!that.context) {
			console.log("Unauthorized request to run MM.");
			app.respond_to_request(res,"Unauthorized request",401);
			app.send_SSE_to_client(that.con_id, JSON.stringify({error: true, MM_hash: that.hash, results: [{dimension_level:"Unauthorized request", value:-1, size:0}]}));
			return;
		}
		//send response to client (results will be sent later using server sent events)
		app.respond_to_request(res);
		global.executed_mms_counter = (global.executed_mms_counter) ? global.executed_mms_counter+1 : 1;
		var mm_as_R_script = utils.contextualizeMMTemplate(RTemplate, req.body.mmParameters, clientAppPath + '/public');
		var data_vectors = await app.process_domain_paths(that.domain_paths, 	that.context, false);
		const r = app.spawn_R_child_process(that);
		app.run_MM_in_R(r, mm_as_R_script, data_vectors);
	}
	app.post(route, returnFunction);
	return returnFunction;
})();

app.listen(config.server_app.port)

/*	called on new data from data source - adds data to context, retrieves and sends data_paths to client 	*/
app.data_callback = function(con_id, auth, data_array_or_object, rm_type_regex, csv = false) {
	app.add_data_to_context(auth, data_array_or_object, csv);
	var new_paths = (csv) ? app.extract_paths_csv(data_array_or_object) : app.extract_paths_openEHR(
		data_array_or_object,
		global.contexts[auth].paths,
		rm_type_regex
	);
	if (new_paths) {
		app.add_paths_to_context(auth, new_paths);
		app.send_SSE_to_client(con_id, JSON.stringify({paths: new_paths}));
	}
}

/*	spawn a new child process interacting with R and returning its results	*/
app.spawn_R_child_process = function(that) {
	//some "best practices" state not to spawn more child processes than available cpu cores ("very bad for performance"). Even with only 2 cores on a notebook and 50+ MMs I cannot confirm this, my performance goes down if I limit the number of parallel running MMs. Maybe this best practice has applications with many parallel users in mind.
	const r = child_process.spawn(config.r.path, ['--vanilla', '--slave', 'LANGUAGE=en']);
	var res = "";
	//listener for output from R, i.e. result of MM
	r.stdout.on('data', (chunk) => {
		res += chunk.toString();
		try {
			if (res.endsWith("<end>")) {
				//remove <end> indicator and catch R's NA and -Inf values, since in JSON they should be null instead
				res = res.replace(/<end>$/, '').replace(/:NA/g, ':null').replace(/:Inf/g, ':null').replace(/:-Inf/g, ':null').replace(/:NaN/g, ':null');
				app.use_result(res, that.context, that.con_id, that.hash);
				//terminate R child process
				r.stdin.write('q()\n');
				//console.log("Finished running MM in R: " + that.hash + " after " + (Date.now()-global.startet_first_MM) + "ms (processed domain_paths for " + global.processed_dps_counter + " MMs, reused for " + (global.executed_mms_counter-global.processed_dps_counter) + " MMs, MMs total:" + global.executed_mms_counter + ")");
			}
		}
		catch (err) {
			console.error(err);
			console.error(res);
			app.use_result(JSON.stringify(err), that.context, that.con_id, that.hash, true);
		}
	});
	//listener for close event
	r.on('close', (code) => {
		if (`${code}`!= 0) {
			console.error(`Exited with code: ${code}`);
			app.use_result(JSON.stringify(r.err_msg), that.context, that.con_id, that.hash, true);
		}
	});
	//listener for error
	r.stderr.on('data', (data) => {
		console.warn(`R-Error: ${data}`);
		r.err_msg += ` ${data}`;
	});
	return r;
}

/*	run MM with data_vectors (aka item0 - item<Y>) in R	*/
app.run_MM_in_R = function(r, mm_as_R_script, data_vectors) {
	data_vectors.forEach((item) => {
		r.stdin.write(item.vector + '\n');
	});
	var lines = mm_as_R_script.split('\n');
	for(var i = 0, le = lines.length; i<le; i++){
		if (lines[i].trim().charAt(0) == '#') continue; //ignore lines containing just comments
		r.stdin.write(lines[i] + '\n');
	}
}

/*	function stores MM-results and sends them back to client	*/
app.use_result = function(res, context, connection_id, mm_hash, add_error_flag = false) {
	//store result for later usage in other MMs
	context["raw_results"][mm_hash] = res;
	//assemble and send response
	var tmp_obj = {MM_hash: mm_hash, results: "<PLACEHOLDER_FOR_RESULTS>"};
	//modify if there was an error in executing MM to be returned
	if (add_error_flag) {
		tmp_obj.error = true;
		tmp_obj.results = [{dimension_level:"<PLACEHOLDER_FOR_RESULTS>", value:-1, size:0}];
	}
	var tmp_envelope = JSON.stringify(tmp_obj);
	tmp_envelope = tmp_envelope.replace('"<PLACEHOLDER_FOR_RESULTS>"', res);
	//send data as server-sent event to client
	app.send_SSE_to_client(connection_id, tmp_envelope);
}

/*	extract values for domainPaths (node paths the MM refers to) and create vactor item<Y> in R
(passing mm_hash is optional. Just for more informative logging in some cases)*/
app.process_domain_paths = function(domain_paths, context, mm_hash) {
	return new Promise(async function(resolve) {
		var data_vectors = await app.try_reusing_vectors(context, domain_paths, mm_hash);
		if (!data_vectors) {
			global.processed_dps_counter = (global.processed_dps_counter) ? global.processed_dps_counter+1 : 1;
			var item_value_retrieval_functions = app.derive_item_value_retrieval_functions(domain_paths);
			input_data = app.extract_data_for_MM_from_dataset(context.data, domain_paths, item_value_retrieval_functions);
			input_data = app.get_other_input_data_for_MM(context, domain_paths, input_data);
			input_data = app.repeat_values_where_applicable(domain_paths, input_data);
			data_vectors = app.create_R_vecors(domain_paths, input_data);
			//unable to extract valid vectors with data, probably because we have some n to m cardinality in nested structure
			if (!data_vectors) {
				app.send_SSE_to_client(req.body.connection_id, JSON.stringify({error: true, MM_hash: mm_hash, results: [{dimension_level:"different_vector_lengths", value:-1, size:0}]}));
				return;
			}
			else {
				app.save_suitable_vectors_for_reuse(context, domain_paths, data_vectors);
			}
		}
		resolve(data_vectors);
	});
}

/*	recursively iterates over dataset, while keeping track of current path and executing given function for each node	*/
app.recursivelyIterateDataset = function(subtree, path, fun = function() {}) {
	//Execute given function for each node in structure
	if (fun(subtree, path)) { //if function returns true - stop going deeper into structure
		return;
	}
	if ((subtree) && (typeof subtree === "object")) {
		if (Array.isArray(subtree)) {
			for (var i = 0, le = subtree.length; i<le; i++) {
				if ((subtree[i]) && (subtree[i].hasOwnProperty("archetype_node_id"))) {
					app.recursivelyIterateDataset(subtree[i], path+"["+subtree[i].archetype_node_id.toLowerCase()+"]", fun);
				} else {
					app.recursivelyIterateDataset(subtree[i], path+"[-1]", fun);
				}
			}
		}
		else {
			for (var key in subtree) {
				var compareKey = key.toLowerCase();
				//for RM attributes like data we need paths ".../data" and ".../data[at0001]"
				if ((subtree[key]) && (subtree[key].hasOwnProperty("archetype_node_id"))) { //kind of suspicious what i did here...
					if (!/\[root\]\[-1\]$/.test(path)) { //but if it is just a new row we don't want that!
						fun(subtree[key], path);
					}
					app.recursivelyIterateDataset(subtree[key], path + "/"+compareKey +"["+subtree[key].archetype_node_id.toLowerCase()+"]", fun);
				} else {
					app.recursivelyIterateDataset(subtree[key], path + "/"+compareKey, fun);
				}
			}
		}
	}
}

/*	extracts openEHR paths from dataset	*/
app.extract_paths_openEHR = function(iData, known_paths, rm_type_regex) {
	var newPaths = {};
	app.recursivelyIterateDataset(iData, "[root]", function(subtree, path) {
		path = path.replace("[root][-1]", utils.placeholders.const_dataset_row);
		path = path.replace("//", "/"); //catch double / introduced in some cases because const for dataset_row contains one
		path = path.replace("[root]", "");
		//add path to tmpDataPaths - only if it has a value and no useless values
		if ((path.length>0) &&
			(path.indexOf('archetype_node_id') == -1) /*&&
			(path.indexOf(']/archetype_details') == -1)*/) {
			if (!known_paths[path]) {
				newPaths[path] = (rm_type_regex.test(path)) ? ""+ subtree : true; //subtree will be a rm_type_name
			}
		}
	});
	return (Object.keys(newPaths).length>0) ? newPaths : false;
}

/*	extracts csv paths from dataset	*/
app.extract_paths_csv = function(data_object) {
	var new_paths = {};
	Object.keys(data_object).forEach((key, i) => {
		new_paths[utils.placeholders.const_dataset_row+key] = true;
	});
	return new_paths;
}

/*	looks up if there are suitable vectors that can be reused (as R-vector string) for MM instead of extracting the data from the dataset - returns false if not or the with vectors (if for all domainPaths there are vectors and all have the same length)	*/
app.try_reusing_vectors = async function(context, dps, mm_hash = false) {
	if (mm_hash) {console.log("Trying to reuse vectors: "+ mm_hash);}
	var data_vectors = [];
	var dp_keys = dps.map((dp) => {
		if (dp.domainPath.indexOf(utils.placeholders.const_other_data_input)>-1) {return false;}
		return utils.generate_hash(JSON.stringify(dp).replace(/"item":"item\d+",/,""));
	});
	dps.forEach((dp, i) => {
		if (!dp_keys[i]) { //we can only reuse if domain_path refers to data
			data_vectors = false;
			return;
		}
		if (!context.vectors[dp_keys[i]]) { //vector does not exist yet = we have to retrieve vectors from dataset
			data_vectors = false;
		}
		else if (context.vectors[dp_keys[i]] == "pending") { //if needed vector is pending - wait for it
			data_vectors = (data_vectors) ? "pending" : false; //if we already know we have to retrieve MM's data, don't wait
		}
		else if ((data_vectors) && (data_vectors!="pending") && ((data_vectors.length == 0) || (data_vectors[0].length==context.vectors[dp_keys[i]].length))) { //until now, all vectors exist and are not pending: as long as length is same for all vectors we can reuse them
			data_vectors.push({vector: context.vectors[dp_keys[i]].vector.replace(/^item\d+/,dp.item), length: context.vectors[dp_keys[i]].length});
		}
		else {
			data_vectors = false;
		}
	});
	// if I need to retrieve vectors from data - mark all not existing vectors as pending (better to do this in the end, to avoid that a bad MM using same vector for two items runs infinite)
	if (!data_vectors) {
		if (mm_hash) {console.log("Have to retrieve data for: "+ mm_hash);}
		dps.forEach((dp, i) => {
			if (!dp_keys[i]) {return;} //only for domain_paths that refer to data
			if (!context.vectors[dp_keys[i]]) {context.vectors[dp_keys[i]] = "pending"}; //if we already have a vector that just does not fit for this MM keep it until this MM replaces it.
		});
	}
	//wait if needed vectors are pending
	if (data_vectors == "pending") {
		if (mm_hash) {console.log("Waiting for a vector: "+ mm_hash);}
		setTimeout(async function() {
			data_vectors = await app.try_reusing_vectors(context, dps, mm_hash);
			if (mm_hash) {console.log("Got pending data vectors: "+ mm_hash);}
		}, 1000);
	}
	if ((mm_hash) && (data_vectors)) {console.log("Reusing data_vectors: "+ mm_hash);}
	return data_vectors;
}

/*	save vectors referring to data as R-vector string for later reuse	*/
app.save_suitable_vectors_for_reuse = function(context, dps, data_vectors) {
	data_vectors.forEach((vec, i) => {
		//obviously only for paths referring to data (not to other MMs' results)
		if (dps[i].domainPath.indexOf(utils.placeholders.const_other_data_input)==-1) {
			context.vectors[utils.generate_hash(JSON.stringify(dps[i]).replace(/"item":"item\d+",/,""))] = vec;
		}
	});
}

/*	function to catch different sized vectors on "dataset row level"
e.g. SELECT e/ehr_id, someDeepPath FROM - than ehr_id shall be "matched" to all someDeepPaths instead of breaking the MM
returns true if we can stop iterating the dataset because there are already incompatible vector lengths	*/
app.catch_different_vector_lengths = function(extracted_data, new_elements_per_vector) {
	//(we can't just look at extracted_data[i].lenght diffs since e.g. item0 one new and item1 two new values in row, would look like item0 no new values and item1 one new)
	//find longest vector
	var max = -1;
	for (var i = 0, le = new_elements_per_vector.length; i<le; i++) {
		//break if there is more than one with more than 1 new value, since we can't be sure which belong together
		if ((max>1) && (new_elements_per_vector[i]>1)) {
			return true;
		}
		else if (new_elements_per_vector[i]>max) {
			max = new_elements_per_vector[i];
		}
	}
	//for each vector
	for (var i = 0, le = new_elements_per_vector.length; i<le; i++) {
		//if vector has no new elements - fill it with "max-times" the NA value
		var valueToAdd = "NA";
		//if vector has one new element - fill it with "max-times" this one value
		if (new_elements_per_vector[i] == 1) {
			valueToAdd = extracted_data[i][extracted_data[i].length-1];
		}
		//now fill if necessary
		for (var i2 = 0, le2 = max - new_elements_per_vector[i]; i2<le2; i2++) {
			extracted_data[i].push(valueToAdd);
		}
	}
	return false;
}

/*	as preparation for MM execution in R - extracts the data for given domain_paths from the given dataset (typically AQL-resultset from a context) using the given item_value_retrieval_functions (one function for each domain_path) into the extracted_data structure	*/
app.extract_data_for_MM_from_dataset = function(dataset, domain_paths, item_value_retrieval_functions) {
	if (!Array.isArray(dataset)) {
		return app.extract_data_for_MM_from_col_based_structure(dataset, domain_paths, item_value_retrieval_functions);
	}
	var extracted_data = [];
	domain_paths.forEach(() => {extracted_data.push([]);});
	dataset.forEach((dataset_row, row_index) => {
		var new_elements_per_vector = domain_paths.map(function() {return 0}); //initialize with 0s
		app.recursivelyIterateDataset(dataset_row, utils.placeholders.const_dataset_row, function(subtree, path) {
			var tmpPath = path.replace("//","/"); //TODO 6 if I would do the path assembling better i would not need to catch //
			var should_stop_following_this_path = true;
			domain_paths.forEach((dP, i) => {
				if (dP.matchedPath == tmpPath) {
					if (subtree) { //catch if subtree is null
						extracted_data[i].push(item_value_retrieval_functions[i](subtree));
						new_elements_per_vector[i]++;
					}
				}
				//there is a domainPath (what MM wants) of which tmpPath (current path in dataset) is the beginning - so we have to iterate deeper into the subtree
				else if (dP.matchedPath.startsWith(tmpPath)) {
					should_stop_following_this_path = false;
				}
			});
			//if current path is not of interest considering domainPaths don't keep going deeper into the structure
			return should_stop_following_this_path;
		});
		if (app.catch_different_vector_lengths(extracted_data, new_elements_per_vector)) {resolve(false);} //after each row check and stop iteration if vector sizes are incompatible
	});
	return extracted_data;
}

/*	e.g. if data is retrieved from CSV-file	its structure is simpler and allowing a method to extract data for MMs	*/
app.extract_data_for_MM_from_col_based_structure = function(dataset, domain_paths, item_value_retrieval_functions) {
	var extracted_data = [];
	domain_paths.forEach((dP, i) => {
		extracted_data.push([]);
		if (dP.matchedPath.indexOf(utils.placeholders.const_other_data_input) == -1) {//only for domain_paths referring to data
			var key = dP.matchedPath.replace(utils.placeholders.const_dataset_row,"");
			dataset[key].forEach((item, ii) => {
				extracted_data[i].push(item_value_retrieval_functions[i](item));
			});
		}
	});
	return extracted_data;
}

/*	For each of the given domain_paths derives a function to use to retrieve data from a dataset, e.g. a method converting a ISO8601 datestring into a timestamp */
app.derive_item_value_retrieval_functions = function(domain_paths) {
	/*	domainPaths: [{domainPath: "path in CIM", pathExtension: "a path extension", matchedPath: "path from dataset", datatype:"R datatype, e.g. numeric, logical, string", item: "item0"}]*/
	var item_value_retrieval_functions = [];
	domain_paths.forEach((dP, i) => {
		item_value_retrieval_functions[i] = function(iItem) {return iItem; }; //default value - just use item.value
		//countAttribute: name, countChildren:  name, asUnixTS, durationIn: comparableUnit
		if (dP.pathExtension){
			//the value to compare in check is whether for this node a given attribute is present or not
			if (dP.pathExtension.countAttribute) {
				//friendly reminder: make sure to create new value instance from dP. ... stuff, else there will be problems with multiple paths in MM when value in itemValueRetrievalFunction is just a pointer ;-)
				var tmp = ''+dP.pathExtension.countAttribute;
				item_value_retrieval_functions[i] = function(iItem) {
					return (iItem.hasOwnProperty(tmp)) ? 1 : 0;
				}
			}
			//the value to compare is how many child nodes of given type are present
			else if (dP.pathExtension.countChildren) {
				//friendly reminder: make sure to create new value instance from dP. ... stuff, else there will be problems with multiple paths in MM when value in itemValueRetrievalFunction is just a pointer ;-)
				var tmp = ''+dP.pathExtension.countChildren;
				item_value_retrieval_functions[i] = function(iItem) {
					var countAnyChild = /.+y_child/.test(tmp); //if predicate is "only_child" or "any_child" any child object will be counted
					var count = 0;
					if (Array.isArray(iItem)) { //iItem is an array
						for (var i2 = 0, le2 = iItem.length; i2<le2; i2++) {
							if  ((countAnyChild) || (iItem[i2].archetype_node_id == tmp)) count++;
						}
					}
					else { //iItem is an object
						if ((countAnyChild) || (iItem.archetype_node_id == tmp)) count++;
					}
					return count;
				}
			}
			//the value to compare is in days from a given date of interest
			else if (dP.pathExtension.asDaysFrom) {
				var date_of_interest = dP.pathExtension.asDaysFrom;
				date_of_interest = Math.round((new Date(date_of_interest)).getTime() / 1000);
				item_value_retrieval_functions[i] = function(iItem) {
					return (-1) * Math.ceil((date_of_interest - Math.round((new Date(iItem)).getTime() / 1000))/(3600 * 24));
				}
			}
			//the value to compare is a UnixTS (i.e. seconds since 1970 in s not in ms)
			else if (dP.pathExtension.asUnixTS) {
				item_value_retrieval_functions[i] = function(iItem) {
					iItem = iItem.replace(/GMT$/,"Z");//fix because ehrbase returns times with GMT ending
					return Math.round((new Date(iItem)).getTime() / 1000);
				}
			}
			//the value to compare shall be a year
			else if (dP.pathExtension.asYear) {
				item_value_retrieval_functions[i] = function(iItem) {
					return new Date(iItem).getFullYear();
				}
			}
			//the value to compare shall be a quarter, e.g. 2019Q3
			else if (dP.pathExtension.asQuarter) {
				item_value_retrieval_functions[i] = function(iItem) {
					var tmp = new Date(iItem);
					return tmp.getFullYear() +' Q'+ (Math.floor(tmp.getMonth()/3)+1);
				}
			}
			//the value to compare shall be a month, e.g. 2019-11
			else if (dP.pathExtension.asMonth) {
				item_value_retrieval_functions[i] = function(iItem) {
					var tmp = new Date(iItem);
					return tmp.getFullYear() +'-'+ (tmp.getMonth()+1);
				}
			}
			//the value to compare shall be a week, e.g. 2019w43
			else if (dP.pathExtension.asWeek) {
				item_value_retrieval_functions[i] = function(iItem) {
					var tmp = new Date(iItem);
					return tmp.getFullYear() +'w'+ (utils.getWeek(tmp));
				}
			}
			//the value to compare shall be a day, e.g. 2019-11-17
			else if (dP.pathExtension.asDay) {
				item_value_retrieval_functions[i] = function(iItem) {
					var tmp = new Date(iItem);
					var tmp_m  = ''+(tmp.getMonth()+1);
					var tmp_d = ''+tmp.getDate();
					if ((tmp_m).length==1) {tmp_m = '0'+tmp_m;}
					if ((tmp_d).length==1) {tmp_d = '0'+tmp_d;}
					return tmp.getFullYear() +'-'+ tmp_m +'-'+ tmp_d;
				}
			}
			//the value to compare shall be the day of the week
			else if (dP.pathExtension.asDayOfWeek) {
				item_value_retrieval_functions[i] = function(iItem) {
					try {
						var tmp = new Date(iItem);
						return ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"][tmp.getDay()];
					}
					catch(error) {
						console.error(error);
					}
				}
			}
			//the value to compare is the duration in given unit (i.e. we have to calculate value in that unit out of ISO8601 duration)
			else if (dP.pathExtension.durationIn) {
				var durationUnit = dP.pathExtension.durationIn;
				item_value_retrieval_functions[i] = function(iItem) {
					//parse value for specified duration unit, i.e. years, month, weeks, days, hours, minutes or seconds
					iItem = iItem.replace(/M(?=.*T)/, "N");  //to make calculation easier just replace month M by N to distinguish it from minutes M
					//first, break everything down to seconds - simplification: 1 year = 365 days, 1 month = 30.416 days
					var matches = iItem.match(/\d+(\.\d+)?[YNWDHMS]/g);
					var tmpVal = 0;
					var inSeconds = {Y: 31536000, N:2627942.4, W: 604800, D:86400, H:3600, M:60, S:1};
					for (var i2 = 0, le2 = matches.length; i2<le2; i2++) {
						var designator = matches[i2][matches[i2].length-1];
						tmpVal+= (matches[i2].replace(designator, "") * inSeconds[designator]);
					}
					//then calculate in desired duration unit
					var inSecondsKeys = {years:"Y", month:"N", weeks:"W", days:"D", hours:"H", minutes:"M", seconds:"S"};
					return tmpVal/inSeconds[inSecondsKeys[durationUnit]];
				}
			}
		}
	});
	return item_value_retrieval_functions;
}

/*	creates string for vector in R (according to datatype expected in domainPath) with values from input_data	*/
app.create_R_vecors = function(domain_paths, input_data) {
	var data_vectors = [];
	domain_paths.forEach((dP, i) => {
		var vec = null;
		//catch null values - can happen when working with MM-results and result is empty
		input_data[i].forEach(function(element, ii) {
			if (element == null) {
				input_data[i][ii] = "NA";
			}
		});
		if (dP.datatype == "numeric") { //just pass the number
			vec = (input_data[i].length == 0) ?
				'item'+ i +' = numeric()' :
				'item'+ i +' = c(' + input_data[i].join(', ') + ')';
		}
		else if (dP.datatype == "string"){ //add " to values
			vec = (input_data[i].length == 0) ?
				'item'+ i +' = factor()' :
				'item'+ i +' = c("' + input_data[i].join('", "') + '")';
		}
		else if (dP.datatype == "logical"){ //ensure values are upperCase (like R expects it)
			vec = (input_data[i].length == 0) ?
				'item'+ i +' = logical()' :
				'item'+ i +' = c(' + input_data[i].map(x=>x.toUpperCase()).join(', ') + ')';
		}
		else { //if nothing is specified, just try to pass value without any actions on it
			vec ='item'+ i +' = c(' + input_data[i].join(', ') + ')';
		}
		vec = vec.replace(/"NA"/g, 'NA');
		data_vectors.push({vector: vec, length: input_data[i].length});
	});
	return data_vectors;
}

/*	retrieves the results of another MM (specified by MMs hash) from the given context	*/
app.get_MMs_results = function(context, hash) {
	if(!context.parsed_results[hash]) {
		context.parsed_results[hash] = JSON.parse(context.raw_results[hash]);
	}
	//catch error entries
	if (Array.isArray(context.parsed_results[hash]) == false) {
		context.parsed_results[hash] = [];
	}
	return context.parsed_results[hash];
}

/*	checks which domain_paths indicate that value should be repeated and for these domain_paths	repeats first value as often as needed to reach same length as longest vector from all domain_paths*/
app.repeat_values_where_applicable = function(domain_paths, input_data) {
	var maxLen = 0;
	var indexesOfVectorsWhereWeHaveToRepeatTheValue = [];
	domain_paths.forEach((dP, i) => {
		if (dP.pathExtension)  {
			//if this flag is set, simply repeat this value until vector has desired length
			if (dP.pathExtension.repeatValue) {
				indexesOfVectorsWhereWeHaveToRepeatTheValue.push(i);
			}
		}
		if (dP.length > maxLen) {maxLen = input_data[i].length;}
	});
	//repeat values for all VectorsWhereWeHaveToRepeatTheValue maxLen-1 times
	indexesOfVectorsWhereWeHaveToRepeatTheValue.forEach(function(vectorIndex) {
		for (var i=0, le=maxLen-1; i<le; i++) {
			input_data[vectorIndex].push(input_data[vectorIndex][0]);
		}
	});
	return input_data;
}

/*	for given domain_path - extracts data from other MMs' results or values provided as inputValues in pathExtension	*/
app.extract_input_data_for_dp = function(dp, context) {
	var input_data = [];
	//for resultsValues and sizeValues retrieve data from stored MM-result
	if ( (dp.pathExtension.asInputValues) &&
			((dp.pathExtension.asInputValues.indexOf(".resultsValue") == 0) ||
			(dp.pathExtension.asInputValues.indexOf(".size") == 0)) ) {
		var myTwoStrings = (dp.pathExtension.asInputValues.indexOf(".resultsValue") == 0) ? [".resultsValue", "value"] : [".size", "size"];
		//extract dimension_level of interest - .resultsValue["aDimensionLevel"] should only extract the value of this DimensionLevel
		var dimension_level = dp.pathExtension.asInputValues.replace(myTwoStrings[0],"").replace("[","").replace("]","");
		//get result of interest
		dp.pathExtension.inputValues.forEach(function(aInputValue) {
			app.get_MMs_results(context, aInputValue).forEach((item) => {
				if ((dimension_level == "") || (item.dimension_level == dimension_level)) {
					input_data.push(item[myTwoStrings[1]]);
				}
			});
		});
	}
	//also for dimension_levels retrieve data from stored MM-result
	else if ((dp.pathExtension.asInputValues) && (dp.pathExtension.asInputValues.indexOf(".dimension_level") == 0)) {
		dp.pathExtension.inputValues.forEach(function(aInputValue) {
			app.get_MMs_results(context, aInputValue).forEach((item) => {
				input_data.push(item.dimension_level);
			});
		});
	}
	//for all others use inputValues
	else {
		input_data = dp.pathExtension.inputValues;
	}
	return input_data;
}

/*	gets other input_data for MM, e.g. data from other MMs' results or values provided as inputValues	*/
app.get_other_input_data_for_MM = function(context, domain_paths, input_data) {
	domain_paths.forEach((dP, i) => {
		if ((dP.pathExtension) && (dP.pathExtension.inputValues)) {
			input_data[i] = app.extract_input_data_for_dp(dP, context);
			//modify if necessary, e.g. if dimension_level looks messy for plot .modify allows to provide JS that manupipulates input_vector in a domainPath
			if (dP.pathExtension.modify) {
				var input_vector = input_data[i];
				eval(dP.pathExtension.modify);
				input_data[i] = input_vector;
			}
		}
	});
	return input_data;
}

/*	creates a new context entry	*/
app.create_empty_context = function(authorization, aql_string) {
	global.contexts[authorization] = {
		aql: aql_string,
		data: [],
		paths: {},
		vectors: {},
		raw_results:{},
		parsed_results:{}
	};
}

/*	adds data to a context	*/
app.add_data_to_context = function(auth, data, csv) {
	global.contexts[auth].vectors = {}; //reset vectors for case that an MM was already executed during query
	if (csv) { //dataset is csv
		global.contexts[auth].data = data;
		global.contexts[auth].csv_data = true;
	}
	else { //dataset is array of rows, e.g. resultset from AQL query on openEHR REST-API
		data.forEach(row => {
				global.contexts[auth].data.push(row);
		});
	}
}

/*	adds paths to a context	*/
app.add_paths_to_context = function(authorization, paths) {
	for (var a_new_path in paths) {
		global.contexts[authorization].paths[a_new_path] = paths[a_new_path];
	}
}

/*	send response for HTTP-Request	*/
app.respond_to_request = function(res, content = null, code = 200) {
	res.statusCode = code;
	res.setHeader('Content-Type', 'application/json');
	if (content === null) {
		res.end();
	}
	else {
		res.end(JSON.stringify(content));
	}
}

/*	connection would be closed after 2 minutes, so server sends a keep_alive message each ~90 seconds	*/
app.keep_alive = function(connection_id) {
	app.send_SSE_to_client(connection_id, "just_keep_me_alive");
	setTimeout(function(){app.keep_alive(connection_id)},90000);
}

/*	send server-sent event to client	*/
app.send_SSE_to_client = function(connection_id, content_to_send) {
	global.connections[connection_id].write('id: ' + (new Date()).toLocaleTimeString() + '\n');
	global.connections[connection_id].write("data: " + content_to_send + '\n\n');
}
